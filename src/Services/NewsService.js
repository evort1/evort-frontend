import Axios from '../Common/axiosConfig';
import lodash from 'lodash';


export const createNews = async (values) => {
  let data;

  try {
    const res = await Axios.post('news/create', values, { headers: { "Content-Type": "multipart/form-data" } })
    data = { message: res.data.message, status: res.status, id: res.data.id };
  } catch (error) {
    data = {message: lodash.capitalize(error.response.data.message), status: error.response.data.status}
  }

  return data;
}

export const findOneNewsByTitle = async (title) => {
  return await Axios.get(`news/title/${title}`);
}


export const deleteNews = async (newsId) => {
  return await Axios.delete(`news/delete/${newsId}`);
}