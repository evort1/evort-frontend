import Axios from '../Common/axiosConfig';
import lodash from 'lodash';

export const createTeam = async (values) => {
  let data;

  try {
    const res = await Axios.post('teams/create', values, { headers: { "Content-Type": "multipart/form-data" } })
    data = { message: res.data.message, status: res.status, id: res.data.id };
  } catch (error) {
    data = {message: lodash.capitalize(error.response.data.message), status: error.response.data.status}
  }

  return data;

}

export const findOneTeamByName = async (teamName) => {
  return await Axios.get(`teams/name/${teamName}`);
}

export const findManyTeamByTeamIds = async (teamIds) => {
  return await Axios.post(`teams/teamId-array/`, teamIds);
}

export const joinTeam = async (teamId, userIds) => {
  let body = [{teamId: teamId},{userIds:userIds} ];

  return await Axios.patch(`teams/${teamId}/join`, body );
}

export const leaveTeam = async (teamId, userId) => {
  return await Axios.patch(`teams/${teamId}/leave/${userId}`);
}

export const updateTeamPoints = async (participants, winner, raw_text_game) => {
  const game = raw_text_game === 'Defence of the Ancient 2' ? 'DefenceOfTheAncient2' : 'MobileLegendBangBang'

  const values = {
    participants, winner, game
  }
  return await Axios.patch(`teams/updateTeamPoints`, values);
}

export const findOneTeamById = async (teamId) => {
  return await Axios.get(`/teams/${teamId}`);
}

export const requestJoinTeam = async (teamId, userId) => {
  return await Axios.patch(`/teams/${teamId}/request/join/${userId}`);
}

export const removeFromPending = async (teamId, userIds) => {
  let body = [{teamId: teamId},{userIds:userIds} ];
  return await Axios.patch(`/teams/${teamId}/request/join/remove/pendingUser`, body)
}

export const deleteTeam = async (teamId) => {
  return await Axios.delete(`/teams/delete/${teamId}`);
}