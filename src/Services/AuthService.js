import Axios from '../Common/axiosConfig';
import lodash from 'lodash';

export const createUser = async (user) => {
  let data;

  try {
    const res = await Axios.post('/Auth/register', user);
    data = { message: res.data.message, status: res.status };
  } catch (error) {
    data = { message: error.response.data.message, status: error.response.status };
  }

  return data;
}

export const authUser = async (user) => {
  let data;
  
  try {
    const res = await Axios.post('/Auth/login', user);
    localStorage.setItem("user", JSON.stringify(res.data));
    data = {message: lodash.capitalize(res.data.message), status: res.data.status};
  } catch (error) {
    data = error.response.data;
  }

  return data;
}

export const checkToken = () => {
  return Axios.get('/Auth/checkToken')
}

export const getCurrentUser = () => {
  return JSON.parse(localStorage.getItem('user'));
}

export const setCurrentUser = (data) => {
  localStorage.setItem("user", JSON.stringify(data));
}

export const logout = () => {
  localStorage.removeItem('user');

  return "User Logout"
}