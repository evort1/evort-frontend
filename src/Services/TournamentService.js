import Axios from '../Common/axiosConfig';
import lodash from 'lodash';

export const createTournament = async (values) => {
	let data;

  try {
		const res = await Axios.post('tournaments/create', values, { headers: { "Content-Type": "multipart/form-data" }});
    data = { message: res.data.message, status: res.status, id: res.data.id };
  } catch (error) {
    data = {message: lodash.capitalize(error.response.data.message), status: error.response.data.status}
	}
	
	return data; 
}

export const updateTournament = async (values, tournamentId) => {
	let data;

	try {
		const res = await Axios.patch(`tournaments/${tournamentId}/edit`, values, { headers: { "Content-Type": "multipart/form-data" }});
    data = { message: res.data.message, status: res.status };
	} catch (error) {
    data = { message: error.response.data.message, status: error.response.status };
	}

	return data;
}

export const updateTournamentStatus = async (status, tournamentId) => {
	let data;

	try {
		const res = await Axios.patch(`tournaments/${tournamentId}/edit/${status}`);
    data = { message: res.data.message, status: res.status };
	} catch (error) {
    data = { message: error.response.data.message, status: error.response.status };
	}

	return data;
}

export const getTournamentDataById = async (tournamentId) => {
	return await Axios.get(`tournaments/${tournamentId}`)
};

export const findManyTournamentsByTournamentIds = async (tournamentIds) => {
  return await Axios.post(`tournaments/tournamentsId-array/`, tournamentIds);
}

export const joinTournament = async (tournamentId, teamIds, userId) => {
	let body = [{tournamentId: tournamentId}, {userId: userId}, {teamIds: teamIds}]
	return await Axios.patch(`tournaments/${tournamentId}/join`, body);
}

export const leaveTournament = async (tournamentId, teamId, userId) => {
	return await Axios.patch(`tournaments/${tournamentId}/leave/${teamId}/${userId}`)
}

export const getCarouselTournamentData = async () => {
	return await Axios.get(`tournaments/carousel`);
}

export const deleteTournament = async (tournamentId) => {
	return await Axios.delete(`tournaments/delete/${tournamentId}`);
}

export const requestJoinTournament = async (tournamentId, teamId) => {
	return await Axios.patch(`/tournaments/${tournamentId}/request/join/${teamId}`)
}

export const removeFromPending = async (tournamentId, teamIds) => {
	let body = [{tournamentId: tournamentId},{teamIds:teamIds} ];
	return await Axios.patch(`/tournaments/${tournamentId}/request/join/remove/pendingTeam`, body)
}

