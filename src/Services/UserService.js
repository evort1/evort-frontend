import Axios from "../Common/axiosConfig";

export const findUserById = async (userId) => {
  return await Axios.get(`user/${userId}`);
}
export const findManyUsersByUserIds = async (userIds) => {
  return await Axios.post(`user/usersId-array/`, userIds);
}

export const updateUserPoints = async (participants, winner, raw_text_game) => {
  const game = raw_text_game === 'Defence of the Ancient 2' ? 'DefenceOfTheAncient2' : 'MobileLegendBangBang'

  const values = {
    participants, winner, game
  }
  return await Axios.patch(`user/updateUserPoints`, values);
}