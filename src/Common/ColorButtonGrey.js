import {withStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

export const ColorButtonGrey = withStyles((theme) => ({
  root: {
    color: theme.palette.getContrastText('#808080'),
    backgroundColor: '#808080',
    '&:hover': {
      backgroundColor: '#808080',
    },
  },
}))(Button);