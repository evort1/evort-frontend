export const slotSizeChoices = [
  {
    value: 4,
    label: '4',
  },
  {
    value: 8,
    label: '8',
  }
];

export const gameChoices = [
  {
    value: 'Defence of the Ancient 2',
    name: 'Dota 2',
  },
  {
    value: 'Mobile Legend Bang Bang',
    name: 'Mobile Legends',
  }
]