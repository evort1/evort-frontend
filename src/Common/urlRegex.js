import { isEmpty } from 'lodash';

export const urlRegex = (url) => {
  const YOUTUBE = "youtube";
  let result;
  let regexToEnableYoutubeUrl = '(.*)&';

  if(isEmpty(url)){
    return url;
  }

  if(!url.match(YOUTUBE)) {
    return url;
  }

  result = url.match(regexToEnableYoutubeUrl);
  if(result) {
    return result[1];
  }

  return url;
}
