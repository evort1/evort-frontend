export const generatedAvatarName = (str) => {
  if(str === false){
    return "";
  }
  
  var nonVowel = str.match(/[^aeiou]/gi);
  var vowel = str.match(/[aeiou]/gi);
  
  if(nonVowel && nonVowel.length < 2){
    if(nonVowel.length === 0){
      return vowel.join("").slice(0, 2).toUpperCase();
    }
    return nonVowel.join("").slice(0, 1).toUpperCase() + vowel.join("").slice(0, 1).toUpperCase();
  }
  return nonVowel === null ? 0 : nonVowel.join("").slice(0, 2).toUpperCase();
}

export const generatedAvatarColor = (str) => {
  if(str === false){
    return "white";
  }

  const ascii = str.split('').map(x => x.charCodeAt(0));
  const color = ["red",
  "pink",
  "purple",
  "deepPurple",
  "indigo",
  "blue",
  "lightBlue",
  "cyan",
  "teal",
  "green",
  "lightGreen" ,
  "lime",
  "yellow",
  "amber",
  "orange",
  "deepOrange",
  "brown", 
  "grey",
  "blueGrey"
]
  const generatedNumber= ascii.reduce((num_1, num_2)=>{ return num_1 + num_2}, 0) % color.length;

  return color[generatedNumber];
}