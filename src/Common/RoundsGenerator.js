const EightSlot = [
  {
    title: 'Round one',
    seeds: [
      {
        id: 1,
        date: new Date().toDateString(),
        teams: [{ name: '' }, { name: '' }],
      },
      {
        id: 2,
        date: new Date().toDateString(),
        teams: [{ name: '' }, { name: '' }],
      },
      {
        id: 3,
        date: new Date().toDateString(),
        teams: [{ name: '' }, { name: '' }],
      },
      {
        id: 4,
        date: new Date().toDateString(),
        teams: [{ name: '' }, { name: '' }],
      },
    ],
  },
  {
    title: 'Round two',
    seeds: [
      {
        id: 1,
        date: new Date().toDateString(),
        teams: [{ name: '' }, { name: '' }],
      },
      {
        id: 2,
        date: new Date().toDateString(),
        teams: [{ name: '' }, { name: '' }],
      },
    ],
  },
  {
    title: 'Round three',
    seeds: [
      {
        id: 3,
        date: new Date().toDateString(),
        teams: [{ name: '' }, { name: '' }],
      },
    ],
  },
];

let fourSlot = [
  {
    title: 'Round one',
    seeds: [
      {
        id: 1,
        date: new Date().toDateString(),
        teams: [{ name: '' }, { name: '' }],
      },
      {
        id: 2,
        date: new Date().toDateString(),
        teams: [{ name: '' }, { name: '' }],
      },
    ],
  },
  {
    title: 'Round two',
    seeds: [
      {
        id: 3,
        date: new Date().toDateString(),
        teams: [{ name: '' }, { name: '' }],
      },
    ],
  },
];


export const shuffleTeam = (array) => {
  array.sort(() => Math.random() - 0.5);
}

export const getBracketSeed = (teamName, date, slotSize) => {
  let teamIndex = 0;
  let dateIndex = 0;
  
  if (slotSize === 4) {
    fourSlot.map(round => {
      round.seeds.map(seed => {
        seed.teams.map(team => {
          team.name = teamName[teamIndex++];
        })
        seed.date = date[dateIndex++];
      })
    })

    return fourSlot;
  }

  if (slotSize === 8) {
    EightSlot.map(round => {
      round.seeds.map(seed => {
        seed.teams.map(team => {
          team.name = teamName[teamIndex++];
        })
        seed.date = date[dateIndex++];
      })
    })

    return EightSlot;
  }

  return null;
}
