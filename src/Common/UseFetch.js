import { useState, useEffect } from 'react';
import axios from 'axios';
import DoReFetch from "./DoReFetch";

const {
  doReFetch
} = DoReFetch;

const useFetch = (url, params) => {
  const [loading, setLoading] = useState(true);
  const [fetchedData, setFetchedData] = useState(null);
  const [reFetch, setReFetch] = useState(true);

  const callReFetch = () => {
    doReFetch(setLoading, setReFetch);
  };

  useEffect(() => {
    const header = {
      headers: { Authorization: `Bearer ${sessionStorage.getItem('token')}`},
      params
    };

    (async () => {
      setReFetch(false);
      try {
        const result = await axios.get(url, header);
        setFetchedData(result.data);
        setLoading(false);
      } catch (e) {
        setLoading(false);
      }
    })();
  }, [reFetch]);

  return { fetchedData, loading, callReFetch}
};

export {
  useFetch
};