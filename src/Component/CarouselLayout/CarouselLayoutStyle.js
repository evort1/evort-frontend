import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  welcome_avatar: props =>({
    color: "black",
    background: props.color,
    width: "300px",
    height: "300px",
    alignSelf: "center",
    margin: "1%",
    border: "10px solid black"
  }),
  welcome_avatar__name: {
    fontSize: '150px',
    color: 'white'
  }
});