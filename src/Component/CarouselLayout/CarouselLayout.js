import React, {useEffect, useState} from 'react'
import './CarouselLayout.css'
import Carousel from 'react-material-ui-carousel';
import RenderCarouselHomePageItem from '../CarouselHomePageItem/RenderCarouselHomePageItem';
import {getCarouselTournamentData} from "../../Services/TournamentService";
import { isEmpty } from 'lodash';

const CarouselLayout = () => {
  const [carouselData, setCarouselData] = useState([]);

  useEffect(() => {
    const getCarouselData = async () => {
      const { data: tempCarouselData } = await getCarouselTournamentData();
      setCarouselData(tempCarouselData);
    }
    getCarouselData();
  }, [])

  return (
    <div className='welcome'>
        <div className='welcome__text'>
          <h1>Welcome to Evort</h1>
        </div>
      {isEmpty(carouselData) ?
          null
        :
      <Carousel className='welcomePage__carousel'>
        {
          carouselData.map( (item, key) =>
            <RenderCarouselHomePageItem key={key} carouselData={item} />
          )
        }
      </Carousel>
      }
    </div>
  )
}

export default CarouselLayout
