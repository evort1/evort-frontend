import React, { Fragment } from 'react';
import { Link } from "react-router-dom";
import './TopThree.css'
import { useFetch } from "../../Common/UseFetch";
import Common from "../../Common/Common";
import UserCard from "../UserCard/UserCard"

const { baseUrl } = Common;

const TopThree = ({game}) => {

  const {
    fetchedData: usersData
  } = useFetch(`${baseUrl}/user/leaderboard/3/${game}`);

  return (
    <div className="TopThree">
      {usersData && usersData.length >= 3 ?
        <Fragment>
          <h1 className="TopThree__title">Top 3 Player {game === 'DefenceOfTheAncient2'? "Dota 2" : "Mobile Legends"}</h1>
          <div className="pos">
            <h2>1st</h2>
            <Link
              className='team__link__noPadding first topThree_team__link'
              to={`/users/${usersData[0]?._id}`}
            >
              <UserCard
                data={usersData[0]}
                game={game}
              />
            </Link>
          </div>
          <div className="oneRow">
            <div className="pos second">
              <h2>2nd</h2>
              <Link
                className='team__link__noPadding second__color topThree_team__link'
                to={`/users/${usersData[1]._id}`}
              >
                <UserCard
                  data={usersData[1]}
                  game={game}
                />
              </Link>
            </div>
            <div className="pos third">
              <h2>3rd</h2>
              <Link
                className='team__link__noPadding third__color topThree_team__link'
                to={`/users/${usersData[2]._id}`}
              >
                <UserCard
                  data={usersData[2]}
                  game={game}
                />
              </Link>
            </div>
          </div>
        </Fragment>
        : null
      }
    </div>
  )
}

export default TopThree;
