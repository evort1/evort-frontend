import React, {useEffect, useState} from 'react';
import {Paper} from '@material-ui/core';
import './RenderCarouselHomePageItem.css';
import CardMedia from '@material-ui/core/CardMedia';
import {Link} from "react-router-dom";

const RenderCarouselHomePageItem =  (data ) => {
  const [carouselImage, setCarouselImage] = useState(null);

  useEffect(() => {
    const carouselImage = data.carouselData.image;
    setCarouselImage(carouselImage);
  }, []);

  return (
    <Link
      className='carousel__link'
      to={`/tournaments/${data.carouselData._id}/overview`}
    >
      <Paper className='carouselHomePage'>
        <h2>{data.carouselData.name}</h2>
        <h3> Prize pool : Rp. {data.carouselData.prizePool}</h3>
        <CardMedia
          className="carousel__cardMedia__image"
          image={carouselImage}
          title={data.carouselData.name}
        />
      </Paper>
    </Link>
  )
}

export default RenderCarouselHomePageItem;