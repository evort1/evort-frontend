import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  search: {
    '& .MuiInputBase-root': {
      color: '#90E0EF'
    }
  },
  form: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '200ch',
      display: "block",
    },
    '& .MuiInputBase-root': {
      color: '#90E0EF'
    }
  },
  tournamentStatusFilter: {
    '& .MuiInputBase-root': {
      width: '100%',
      color: '#90E0EF'
    }
  },
  ul: {
    "& .MuiPaginationItem-root": {
      color: "#FFFFFF"
    }
  },
  root: {
    display: 'flex',
    '& > * + *': {
      marginLeft: theme.spacing(2),
    },
  },
}));