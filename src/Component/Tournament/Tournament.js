import React, { useState, Fragment } from 'react'
import TournamentCard from '../TournamentCard/TournamentCard'
import './Tournament.css'
import { useFetch } from "../../Common/UseFetch";
import Common from "../../Common/Common";
import { Link, useHistory } from "react-router-dom";
import Button from "@material-ui/core/Button";
import CustomTextField from '../CustomMaterialUI/CustomTextField';
import MenuItem from '@material-ui/core/MenuItem';
import { useStyles } from './TournamentStyle';
import Pagination from '@material-ui/lab/Pagination';
import CircularProgress from "@material-ui/core/CircularProgress";

const {
  baseUrl
} = Common;

const Tournament = () => {
  const history = useHistory();
  const classes = useStyles();

  const [tournamentStatus, setTournamentStatus] = useState("all");
  const [tournamentSearchKeyword, setTournamentSearchKeyword] = useState("");
  const [tournamentGame, setTournamentGame] = useState("all");
  const [page_, setPage_] = useState(1);

  const params = {
    name: tournamentSearchKeyword,
    status: tournamentStatus,
    game: tournamentGame
  };

  const {
    fetchedData: data, callReFetch
  } = useFetch(`${baseUrl}/tournaments/pagination/${page_}/10/search/by`, params);

  const tournamentStatusChoices = [
    { status: 'upcoming' },
    { status: 'ongoing' },
    { status: 'completed' },
    { status: 'all' },
  ]

  const gameChoices = [
    { value: 'Defence of the Ancient 2', game: 'Defence of the Ancient 2' },
    { value: 'Mobile Legend Bang Bang', game: 'Mobile Legend Bang Bang'},
    { value: 'all', game: 'all'}
  ]

  const handleOnChangeSearchField = (e) => {
    setPage_(1);
    setTournamentSearchKeyword(e);
    callReFetch();
  }
  
  const handleOnChangeTournamentFilterByStatus = (e) => {
    setPage_(1);
    setTournamentStatus(e)
    callReFetch();
  }

  const handleOnChangeTournamentFilterByGame = (e) => {
    setPage_(1);
    setTournamentGame(e)
    callReFetch();
  }

  const handleChangePagination = (event, page) => {
    setPage_(page)
    callReFetch()
  }

  return (
    <div className='tournament'>
      <div className='tournament_row'>
        <h1>List of Tournaments</h1>
        <div className='tournament__createButton'>
          <Button
            className='createButton'
            variant='outlined'
            color='primary'
            onClick={() => history.push('/createTournament')}
          >
            Create Tournament
          </Button>
        </div>
      </div>
      <div className='filterNameAndStatus'>
        <div className={classes.search}>
          <CustomTextField
            name='searchBar'
            label="Find a Tournament"
            size="medium"
            variant="outlined"
            onChange={ (e) => handleOnChangeSearchField(e.target.value) }
            value={tournamentSearchKeyword}
          />
        </div>
        <form className={classes.form}>
          <div className='tournament_row'>
            <CustomTextField
              name='tournamentStatusFilter'
              select
              label="Status Filter"
              variant="outlined"
              size="medium"
              value={tournamentStatus}
              onChange={ (e) => handleOnChangeTournamentFilterByStatus(e.target.value)}
              style={{ width: 200 }}
              className={classes.tournamentStatusFilter}
            >
              {tournamentStatusChoices.map((option) => (
                <MenuItem key={option.status} value={option.status}>
                  {option.status}
                </MenuItem>
              ))}
            </CustomTextField>
            <CustomTextField
              name='tournamentGameFilter'
              select
              label="Game Filter"
              variant="outlined"
              size="medium"
              value={tournamentGame}
              onChange={ (e) => handleOnChangeTournamentFilterByGame(e.target.value)}
              style={{ width: 200 }}
              className={classes.tournamentGameFilter}
            >
              {gameChoices.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.game}
                </MenuItem>
              ))}
            </CustomTextField>
          </div>
        </form>
      </div>
      {data ?
        <Fragment>
          {data.tournaments.length != 0 ?
            <div className='tournament__list'>
              {data.tournaments.map((tournamentData, index) => (
                <Link
                  className='tournament__link'
                  key={index}
                  to={`/tournaments/${tournamentData._id}/overview`}
                >
                  <TournamentCard
                    name={tournamentData.name}
                    description={tournamentData.description}
                    image={tournamentData.image}
                  />
                </Link>
              ))}
            </div>
            : <h1 className='notFound'> Tournament not found </h1>
          }
          <div className="news__pagination">
            <Pagination 
              className={classes.ul} 
              count={data.end} 
              page={page_}onChange={(event, page) =>handleChangePagination(event, page)} 
              color="primary" 
              shape="rounded" 
            />
          </div>
        </Fragment>
        :
        <div className='loading'>
          <div className={classes.root}>
            <CircularProgress size={80}/>
          </div>
        </div>
      }
    </div>
  )
}

export default Tournament;
