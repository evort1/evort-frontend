import React, {useEffect, useState} from 'react'
import './UserCard.css'
import Avatar from "@material-ui/core/Avatar";
import {generatedAvatarColor, generatedAvatarName} from "../../Common/generateFromUsername";
import {useStyles} from "../../Page/ProfilePage/ProfilePageStyle";

const UserCard = ({ data, game }) => {

  console.log(data);
  const props = data ? { color: generatedAvatarColor(data?.username) } : { color: "white" } ;
  const classes = useStyles(props);
  const [points, setPoints] = useState([]);

  useEffect(() => {
    const getPoints = () => {
      setPoints(data.points[game]);
    }

    getPoints();
  },[])

  return (
    <div className='userCard'>
      {data ?
        <div>
          <div className='profilePagePicture'>
            <Avatar alt={data?.username} className={classes.profilePage__avatar}>
                <span className={classes.profilePage__avatar__name} >
                  {generatedAvatarName(data.username)}
                </span>
            </Avatar>
          </div>
          <div className='userName'>
            <h2>{data.name}</h2>
          </div>
          <div className='points'>
            <h3>
              Points: {points}
            </h3>
          </div>
        </div>
        :
        null
      }
    </div>
  )
}

export default UserCard;
