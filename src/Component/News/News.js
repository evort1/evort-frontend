import React, { Fragment, useState } from 'react';
import './News.css';
import { useFetch } from "../../Common/UseFetch";
import Common from "../../Common/Common";
import NewsCard from '../NewsCard/NewsCard'
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import {useHistory} from "react-router-dom";
import Pagination from '@material-ui/lab/Pagination';
import { useStyles } from './NewsStyle';
import CircularProgress from '@material-ui/core/CircularProgress';
const { baseUrl } = Common;

const News = ({adminAuthority}) => {
  const [page_, setPage_] = useState(1);

  const history = useHistory();
  const {fetchedData: data , callReFetch} = useFetch(`${baseUrl}/news/${page_}/5`);
  const classes = useStyles();

  const handleChangePagination = (event, page) => {
    setPage_(page)
    callReFetch()
  }

  return (
    <div className='news'>
      <div className='news__header'>
        <h1>Hot and Trending News</h1>
      </div>
      <div>
        {adminAuthority ?
          <div className='createNewsButton__row'>
            <Button
              className='createNewsButton'
              color='primary'
              variant='outlined'
              onClick={() => history.push('/createNews')}
            >
              create News
            </Button>
          </div>
          : null
        }
      </div>
      {data ?
        <Fragment>
          {data.news.length != 0 ?
            <div className='news__list'>
              {data.news.map((newsData, index) => (
                <Link
                  className='news__link'
                  key={index}
                  to={`/news/${newsData._id}`}
                >
                  <NewsCard
                    title={newsData.title}
                    body={newsData.body}
                    image={newsData.image}
                    date={newsData.created_at}
                  />
                </Link>
              ))}
            </div>
            : <h1 className='notFound'> News not found </h1>
          }

          <div className="news__pagination">
            <Pagination
              className={classes.ul}
              count={data.end}
              onChange={(event, page) =>handleChangePagination(event, page)}
              color="primary"
              shape="rounded"
            />
          </div>
        </Fragment>
        :
        <div className='loading'>
          <div className={classes.root}>
            <CircularProgress size={80}/>
          </div>
        </div>
      }
    </div>
  )
}

export default News;
