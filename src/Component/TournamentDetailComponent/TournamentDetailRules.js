import React from 'react'
import {convertToHTML} from "draft-convert";
import {convertFromRaw} from "draft-js";
import parse from 'html-react-parser';

const TournamentDetailRules = ({rules}) => {
  return (
    <div >
      <h3> {parse(convertToHTML(convertFromRaw(JSON.parse(rules))))} </h3>
    </div>
  )
}

export default TournamentDetailRules
