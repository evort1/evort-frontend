import React, { useEffect, useState, Fragment } from 'react';
import './TournamentDetailBracket.css'
import { Bracket } from 'react-brackets';
import { getBracketSeed } from '../../../Common/RoundsGenerator';
import Button from "@material-ui/core/Button";
import EditTournamentBracketModal from "../../../Modal/EditTournamentBracketModal/EditTournamentBracketModal";
import { findManyTeamByTeamIds, findOneTeamById } from '../../../Services/TeamService';
import { useParams } from 'react-router';
import { updateTournament } from '../../../Services/TournamentService';
import ClearTournamentBracketModal from "../../../Modal/ClearTournamentBracketModal/ClearTournamentBracketModal";
import { isEqual } from 'lodash';

const TournamentDetailBracket = ({ teamIds, bracketSeedsIds, dateSeeds, slotSize, status, winnerId }) => {
  const isSubStringAllNull = (array, len) => {
    let index = 0;
    let isAllNull = true;
    while(index < len && isAllNull){
      if (array[index] !== "null") isAllNull = false;
      index++;
    }

    return isAllNull;
  }

  const [openClearTournamentBracketModal, setOpenClearTournamentBracketModal] = useState(false);
  const [openEditTournamentBracket, setOpenEditTournamentBracket] = useState(false);
  const [teamsData, setTeamsData] = useState([]);
  const [winnerName, setWinnerName] = useState(null);
  const [teamsAutocomplete, setTeamsAutocomplete] = useState([]);
  const [bracketSeeds, setBracketSeeds] = useState([]);
  const { tournamentId } = useParams();

  const disabledFirstMatch = (!isSubStringAllNull(bracketSeedsIds, slotSize));

  useEffect(() => {
    const getWinnerData = async () => {
      const winner = winnerId ? await findOneTeamById(winnerId) : null;

      if(winner){
        setWinnerName(winner.data.name);
      }
    }

    const fetchData = async () => {
      const { data: teamData } = await findManyTeamByTeamIds(teamIds);
      const { data: bracketSeeds } = await findManyTeamByTeamIds(bracketSeedsIds);

      const tempTeam = teamData.map(team => ({name: team.name, data: team.name}));
      const tempBracketSeeds = bracketSeeds.map(seed => seed ? seed.name : null);
      tempTeam.push({name: "-----------", data: null});

      setTeamsData(teamData);
      setTeamsAutocomplete(tempTeam);
      setBracketSeeds(tempBracketSeeds);
    };

    fetchData();
    getWinnerData();
  }, [])

  const handleClickOpen = () => {
    setOpenEditTournamentBracket(true);
  };

  const handleClickClose = () => {
    setOpenEditTournamentBracket(false);
    // eslint-disable-next-line no-restricted-globals
    location.reload();
  };

  const handleClickClear = async () => {
    const slot = slotSize === 4 ? 6 : 14;

    const empty = Array(slot).fill("null");
    bracketSeedsIds = empty;

    let bodyFormData = new FormData();

    bracketSeedsIds.map(team => {
      bodyFormData.append("bracketSeeds", team);
    })

    const { status: updateTournamentBracket } = await updateTournament(bodyFormData, tournamentId);

    if (updateTournamentBracket === 200) {
      // eslint-disable-next-line no-restricted-globals
      location.reload();
    }
  }

  const handleCloseClearTournamentBracketModal = () => {
    setOpenClearTournamentBracketModal(false);
  }

  const handleOpenClearTournamentBracketModal = () => {
    setOpenClearTournamentBracketModal(true);
  }

  return (
    <div>
      <h3>Bracket</h3>
      <Bracket
        rounds={getBracketSeed(bracketSeeds, dateSeeds, slotSize)}
      />
      {status !== 'upcoming' ? 
        <Fragment>
          { winnerName ? 
            <div className="winner">
              <h3>Winner</h3>
              <p>{winnerName}</p>
            </div>
            :
            <div className="winner">
              <h3>Winner</h3>
              <p>You haven't choose the winner</p>
            </div>
          }
        </Fragment>
        : null
      }
      {isEqual(status, 'ongoing') ?
        <Fragment>
          <Button className='editTournamentBracketButton' variant="outlined" color="primary" onClick={handleClickOpen}>
            Edit Bracket
          </Button>
          <Button className='editTournamentBracketButton' variant="outlined" color="secondary" onClick={handleOpenClearTournamentBracketModal}>
            Clear Bracket
          </Button>
          <ClearTournamentBracketModal
            handleClickClear={handleClickClear}
            openClearTournamentBracketModal={openClearTournamentBracketModal}
            handleCloseClearTournamentBracketModal={handleCloseClearTournamentBracketModal}
          />
        </Fragment>
        : null
      }
      {bracketSeeds.length !== 0 ?
        <EditTournamentBracketModal
          open={openEditTournamentBracket}
          handleClose={handleClickClose}
          bracketSeeds={bracketSeeds}
          disabledFirstMatch={disabledFirstMatch}
          dateSeeds={dateSeeds}
          teamsAutocomplete={teamsAutocomplete}
          teamsData={teamsData}
          slotSize={slotSize}
          winnerName={winnerName}
        />
        : null
      }
    </div>
  )
}

export default TournamentDetailBracket
