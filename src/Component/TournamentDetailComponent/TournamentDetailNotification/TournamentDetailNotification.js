import React, {Fragment, useEffect, useState} from 'react'
import { isEqual, isEmpty } from 'lodash';
import {useStyles} from "./TournamentDetailNotificationStyles";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";
import {getCurrentUser} from "../../../Services/AuthService";
import {joinTournament, removeFromPending} from "../../../Services/TournamentService";
import {findManyTeamByTeamIds, joinTeam} from "../../../Services/TeamService";
import {DataGrid} from "@material-ui/data-grid";
import Pagination from "@material-ui/lab/Pagination";
import PaginationItem from "@material-ui/lab/PaginationItem";
import './TournamentDetailNotification.css';

const TournamentDetailNotification = ({tournamentId, pendingTeamIds, isCreator}) => {

  const user = getCurrentUser();
  const classes = useStyles();
  const history = useHistory();
  const [rowData, setRowData] = useState([]);
  const [select, setSelection] = useState([]);

  useEffect(() => {
    const fetchingPendingTeamRowData = async () => {
      let tempRow = [];
      const {data: pendingTeamsData} = await findManyTeamByTeamIds(pendingTeamIds);
      isEmpty(pendingTeamsData) ?
        tempRow = []
        :
        pendingTeamsData.map((pendingTeamData, key) => {
          tempRow.push({id:key+1, teamName: pendingTeamData.name, _id: pendingTeamData._id});
        })
      setRowData(tempRow);
    };

    fetchingPendingTeamRowData();
  },[])

  const columns = [
    { field: 'id', width: 100,
      cellClassName: 'super-app-theme--cell',
      headerClassName: 'super-app-theme--header',
      renderHeader: () => (<strong> {'No'} </strong>)
    },
    { field: 'teamName', width: 250,
      cellClassName: 'super-app-theme--cell',
      headerClassName: 'super-app-theme--header',
      renderHeader: () => (<strong> {'Team Name'} </strong>)
    }
  ];

  const CustomPagination = (props) => {
    const { pagination, api } = props;
    return (
      <Pagination
        color="primary"
        variant="outlined"
        shape="rounded"
        size="large"
        page={pagination.page}
        count={pagination.pageCount}
        renderItem={(props2) => <PaginationItem className={classes.pagination1}{...props2} disableRipple />}
        onChange={(event, value) => api.current.setPage(value)}
      />
    );
  }

  const handleAcceptTournament = async () => {
  let selectedTeamIds = []
    await Promise.all(
      rowData.map( async (pendingTeamData) => {
        select.map( async (selected) => {
          if(pendingTeamData.id == selected ){
            selectedTeamIds.push(pendingTeamData._id);
          }
        })
      })
    )

    const {data: joinTournamentResponses} = await joinTournament(tournamentId, selectedTeamIds, user?.data._id);
    await removeFromPending(tournamentId, selectedTeamIds );

    await joinTournamentResponses.map( (eachResponse) => {
      alert(eachResponse.message);
    })

    history.push(`/tournaments/${tournamentId}/overview`);
    // eslint-disable-next-line no-restricted-globals
    location.reload();
  };

  const handleRejectTournament = async () => {
    let selectedTeamIds = []
    await Promise.all(
      rowData.map( async (pendingTeamData) => {
        select.map( async (selected) => {
          if(pendingTeamData.id == selected ){
            selectedTeamIds.push(pendingTeamData._id);
          }
        })
      })
    )

    const {data: removeTournamentResponses} = await removeFromPending(tournamentId, selectedTeamIds );

    await removeTournamentResponses.map( (eachResponse) => {
      alert(eachResponse.message);
    })

    history.push(`/tournaments/${tournamentId}/overview`);
    // eslint-disable-next-line no-restricted-globals
    location.reload();
  }

  return (
    <div className='TournamentDetailNotification'>
      {isCreator ?
        <div>
          {isEmpty(rowData) ?
            <h2>There is Currently No Notification</h2>
            :
            <div>
              <div className='tournamentDetailNotification__header'>
                <h2>Teams who's Requesting to Join your tournament</h2>
              </div>
              <div className='dataGrid__tournamentDetailNotification'>
                <div style={{ height: 400, width: '80%'}} className={classes.root}>
                  <DataGrid
                    rows={rowData}
                    columns={columns}
                    pageSize={5}
                    checkboxSelection
                    components={{
                      pagination: CustomPagination,
                    }}
                    onSelectionChange={(newSelection) => {
                      setSelection(newSelection.rowIds);
                    }}
                  />
                  <Button
                    className='rejectTournamentNotificationButton'
                    variant="contained"
                    color="secondary"
                    onClick={handleRejectTournament}
                  >
                    Reject
                  </Button>
                  <Button
                    className='acceptTournamentNotificationButton'
                    variant="contained"
                    color="primary"
                    onClick={handleAcceptTournament}
                  >
                    Accept
                  </Button>
                </div>
              </div>
            </div>
          }
        </div>
        :
        <h2 className='tournamentNotJoined__text'> This page is only accessible for the creator of the tournament</h2>
      }
    </div>
  )
}

export default TournamentDetailNotification;