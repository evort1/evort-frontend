import React, { Fragment } from 'react'
import './TournamentDetailOverview.css'
import moment from 'moment';
import formatter from "../../Common/converterIntToCurrencyIDR";
import ReactPlayer from 'react-player';
import { isEmpty } from 'lodash';
import Common from "../../Common/Common";
import { useFetch } from "../../Common/UseFetch";
import { Link } from "react-router-dom";
import TeamCard from "../TeamCard/TeamCard"
import {convertFromRaw} from "draft-js";
import {convertToHTML} from "draft-convert";
import parse from 'html-react-parser';

const {
  baseUrl
} = Common;

const TournamentDetailOverview = ({ description, endDate, startDate, slotSize, entryFee, teams, url, winner, status, game }) => {
  const startDateFormatted = moment(endDate).format('LLLL');
  const endDateFormatted = moment(startDate).format('LLLL');

  const { fetchedData: winnerTeam } = useFetch(`${baseUrl}/teams/${winner}`);


  return (
    <div className='TournamentDetailOverview'>
      {status === 'completed' ?
        <div>
          {winnerTeam ?
            <div className='TournamentDetailOverview__winner'>
              <h3 className="TournamentDetailOverview__winner__text">Tournament Winner</h3>
              <Link
                className='team__link'
                to={`/teams/${winnerTeam._id}/overview`}
              >
                <TeamCard
                  name={winnerTeam.name}
                  description={winnerTeam.description}
                  image={winnerTeam.image}
                  points={winnerTeam.points}
                />
              </Link>
            </div>
            : null
          }
        </div>
        :
        null
      }
      <div>
          <h3>Game</h3>
          <h1>{game === 'Defence of the Ancient 2' ? 'Dota 2' : 'Mobile Legends'}</h1>
      </div>
      <div className='TournamentDetailOverview__row'>
        <div>
          <h3>Slot</h3>
          <p>{teams.length} / {slotSize} ({slotSize - teams.length} Slot Available)</p>
        </div>
        <div>
          <h3>Entry Fee</h3>
          <p>{formatter.format(entryFee)}</p>
        </div>
        <div>
          <h3>Schedule</h3>
          <p>Start: {startDateFormatted}</p>
          <p>End: {endDateFormatted}</p>
        </div>
      </div>
      <Fragment>
        {isEmpty(url) ?
          null
          :
          <ReactPlayer url={url} className='TournamentDetailOverview__player' />
        }
      </Fragment>
      <div className='TournamentDetailOverview__row'>
        <div>
          <h3>Tournament Info</h3>
          <p>{parse(convertToHTML(convertFromRaw(JSON.parse(description))))}</p>
        </div>
      </div>
    </div>
  )
}

export default TournamentDetailOverview
