import React, { useEffect, useState } from 'react'
import { Link } from "react-router-dom";
import TeamCard from "../TeamCard/TeamCard"
import { findManyTeamByTeamIds } from '../../Services/TeamService'
import { isEmpty } from 'lodash';

const TournamentDetailTeams = ({ teamIds }) => {
  const [teams, setTeams] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const result = await findManyTeamByTeamIds(teamIds);
      setTeams(result.data);
    };

    fetchData();
  }, [])

  return (
    <div className='TournamentDetailTeams'>
      {isEmpty(teams) ? 
        <p>No Team Joined yet</p> 
        :
        <div className='team__list'>
          {teams.map((teamData, index) => (
            <Link
              className='team__link'
              key={index}
              to={`/teams/${teamData._id}/overview`}
            >
              <TeamCard
                name={teamData.name}
                description={teamData.description}
                image={teamData.image}
                points={teamData.points}
              />
            </Link>
          ))}
        </div>
      }
    </div>
  )
}

export default TournamentDetailTeams
