import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  topNavigationBar__avatar: props =>({
    color: "black",
    background: props.color,
    '&:hover': {
      background: "#90E0EF",
   },
  }),
  topNavigationBar__avatar__name: {
    fontSize: '20px',
    fontWeight: 'bold',
  },
});