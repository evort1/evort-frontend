import React, { useState, useEffect } from 'react'
import Avatar from '@material-ui/core/Avatar';
import './TopNavigationBar.css'
import {
  Link, useHistory
} from "react-router-dom";
import { logout } from '../../Services/AuthService';
import { useStyles } from "./TopNavigationBarStyle";
import { generatedAvatarColor, generatedAvatarName } from '../../Common/generateFromUsername'
import _ from 'lodash';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const TopNavigationBar = ({ user, setUser, setAuthStatus, AuthStatus }) => {
  const [username, setUsername] = useState("");
  const [anchorEl, setAnchorEl] = React.useState(null);
  const history = useHistory();

  useEffect(() => {
    _.isEmpty(user) ? setUsername("") : setUsername(user.data.username)
  }, [AuthStatus, user]);

  const handleLogOut = () => {
    alert(logout());
    setUser({});
    history.push('/');
    setAuthStatus(false);
    setAnchorEl(null);
  }

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleProfile = () => {
    history.replace({ pathname: `/users/${user?.data._id}` });
    setAnchorEl(null);
  }

  const props = AuthStatus ? { color: generatedAvatarColor(username) } : { color: "white" };
  const classes = useStyles(props);

  return (
    <div className='topNavigationBar'>
      <div className='topNavigationBar__left'>
        <Link className='topNavigationBar__Link evortLogo' to="/">
          <h4>EVORT</h4>
        </Link>
        <Link className='topNavigationBar__Link' to="/tournaments">
          <h4>Tournaments</h4>
        </Link>
        <Link className='topNavigationBar__Link' to="/teams">
          <h4>Teams</h4>
        </Link>
        <Link className='topNavigationBar__Link' to="/news">
          <h4>News</h4>
        </Link>
        <Link className='topNavigationBar__Link' to="/leaderboard">
          <h4>Leaderboard</h4>
        </Link>
      </div>
      <div className="topNavigationBar__right">
        {AuthStatus ?
          <div className='topNavigationBar__Link'>
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
              <Avatar alt={username} className={classes.topNavigationBar__avatar}>
                <span className={classes.topNavigationBar__avatar__name} >
                  {generatedAvatarName(username)}
                </span>
              </Avatar>
            </Button>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              <MenuItem onClick={handleProfile}>Profile</MenuItem>
              <MenuItem onClick={handleLogOut}>Logout</MenuItem>
            </Menu>
          </div>
          :
          <Link className='topNavigationBar__Link' to="/login">
            <h4>LOGIN</h4>
          </Link>
        }
      </div>
    </div>
  )
}

export default TopNavigationBar
