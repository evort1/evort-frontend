import React from 'react'
import { Redirect, Route } from 'react-router-dom';
import { getCurrentUser } from '../Services/AuthService';

const PrivateRoute = ({ component: Component, ...rest }) => {
  const data = getCurrentUser()
  return (
    <Route
      {...rest}
      render={({props, location}) =>
        data ? 
        <Component {...props} />
        : 
        <Redirect   to={{
          pathname: "/login",
          state: { 
            from: location,
            message: 'You should login first before access that routes' 
          }
          }} 
        /> 
      }
    />
  );
}

export default PrivateRoute
