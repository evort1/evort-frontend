import { useFetch } from "../../Common/UseFetch";
import {Link, useHistory} from "react-router-dom";
import TeamCard from "../TeamCard/TeamCard"
import React, {useState, Fragment} from "react";
import Common from "../../Common/Common";
import './Team.css'
import Button from "@material-ui/core/Button";
import Pagination from '@material-ui/lab/Pagination';
import CustomTextField from "../CustomMaterialUI/CustomTextField";
import {useStyles} from "../Tournament/TournamentStyle";
import CircularProgress from "@material-ui/core/CircularProgress";
const {
  baseUrl
} = Common;

const Team = () => {
  const history = useHistory();
  const classes = useStyles();

  const [teamSearchKeyword, setTeamSearchKeyword] = useState("");
  const [page_, setPage_] = useState(1);

  const params = {
    name : teamSearchKeyword
  }
  const {
    fetchedData: data, callReFetch
  } = useFetch(`${baseUrl}/teams/pagination/${page_}/10/search/by`, params);

  const handleOnChangeSearchField = (e) => {
    setTeamSearchKeyword(e);
    setPage_(1)
    callReFetch();
  }

  const handleChangePagination = (event, page) => {
    setPage_(page)
    callReFetch()
  }

  return (
    <div className='team'>
      <div className='team_row'>
        <h1>List of Teams</h1>
        <div className='team__createButton'>
          <Button
            className='createButton'
            variant='outlined'
            color='primary'
            onClick={() => history.push('/createTeam')}
          >
            Create Team
          </Button>
        </div>
      </div>
      <div className='Team__searchBar'>
        <div className={classes.search}>
          <CustomTextField
            name='searchBar'
            label="Find a Team"
            size="medium"
            variant="outlined"
            onChange={ (e) => handleOnChangeSearchField(e.target.value) }
            value={teamSearchKeyword}
          />
        </div>
      </div>
      {data ?
        <Fragment>
          {data.teams.length != 0 ?
            <div className='team__list'>
              {data.teams.map ((teamData, index) => (
                <Link
                  className='team__link'
                  key={index}
                  to={`/teams/${teamData._id}/overview`}
                >
                  <TeamCard
                    name={teamData.name}
                    description={teamData.description}
                    image={teamData.image}
                  />
                </Link>
              ))}
            </div>
            : <h1 className='notFound'> Team not found </h1>
          }
          <div className="news__pagination">
            <Pagination 
              className={classes.ul} 
              count={data.end} 
              page={page_}
              onChange={(event, page) =>handleChangePagination(event, page)}
              color="primary" 
              shape="rounded" 
            />
          </div>
        </Fragment>
        :
        <div className='loading'>
          <div className={classes.root}>
            <CircularProgress size={80}/>
          </div>
        </div>
      }
    </div>
  )
}

export default Team;