import { makeStyles } from '@material-ui/core/styles';

export const neonBlueStyle = makeStyles(({
  notchedOutline: {
    borderWidth: '1px',
    borderColor: '#90E0EF',
  },
  root: {
    '&$disabled $notchedOutline': {
      borderColor: '#D3D3D3'
    },
    width:'100%'
  },
  disabled: {}
}));