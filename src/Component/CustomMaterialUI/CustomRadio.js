import React from 'react';
import Radio from '@material-ui/core/Radio';
import { withStyles } from '@material-ui/core/styles';

const NeonBlueRadio = withStyles({
  root: {
    color: '#90E0EF',
    '&$checked': {
      color: '#90E0EF',
    },
  },
  checked: {},
})((props) => <Radio color="default" {...props} />);

export default NeonBlueRadio;