import React from 'react';
import { neonBlueStyle } from "./NeonBlueOutlineStyle";
import { TextField } from '@material-ui/core'

const CustomTextField = props => {
  const classes = neonBlueStyle();

  return (
    <div>
      <TextField 
        {...props} 
        InputLabelProps={{
          style: { color: '#90E0EF' }
        }}
        InputProps={{
          classes: {
            root: classes.root,
            notchedOutline: classes.notchedOutline,
            disabled: classes.disabled,
          }
        }}
        className={classes.disable}
      />
    </div>
  )
}

export default CustomTextField
