import React from 'react';
import { neonBlueStyle } from "./NeonBlueOutlineStyle";
import { DatePicker } from '@material-ui/pickers';

const CustomTextField = props => {
  const classes = neonBlueStyle();

  return (
    <div>
      <DatePicker
        {...props}
        InputLabelProps={{
          style: { color: '#90E0EF' }
        }}
        InputProps={{
          classes: {
            notchedOutline: classes.notchedOutline,
          }
        }}
      />
    </div>
  )
}

export default CustomTextField
