import React, {useEffect, useState} from 'react'
import './TournamentDetailNavigationBar.css'
import {
  Switch,
  Route,
  NavLink,
  useRouteMatch
} from "react-router-dom";
import TournamentDetailOverview from '../TournamentDetailComponent/TournamentDetailOverview'
import TournamentDetailBracket from '../TournamentDetailComponent/TournamentDetailBracket/TournamentDetailBracket'
import TournamentDetailTeams from '../TournamentDetailComponent/TournamentDetailTeams'
import TournamentDetailRules from '../TournamentDetailComponent/TournamentDetailRules'
import TournamentDetailNotification
  from "../TournamentDetailComponent/TournamentDetailNotification/TournamentDetailNotification";
import {getCurrentUser} from "../../Services/AuthService";
import { isEqual, isEmpty } from 'lodash';

const TournamentDetailNavigationBar = ({data}) => {
  let { url, path } = useRouteMatch();
  const { teams, bracketSeeds, dateSeeds,
    slotSize, status, rules, winner,
    pendingTeam:pendingTeamIds, _id:tournamentId,
    createdBy: tournamentCreatorId
  } = data
  const user = getCurrentUser();
  const [isCreator, setIsCreator] = useState(false);


  useEffect(() => {
    const verifyIsCreator = async () => {
      if (isEqual(tournamentCreatorId, user?.data._id)) {
        setIsCreator(true);
      }
    }
    verifyIsCreator();
  }, [])

  return (
    <div className='TournamentDetailNavigationBar'>
      <div className='TournamentDetailNavigationBar__Links'>
        <NavLink className={'TournamentDetailNavigationBar__Link'} activeClassName='is-active' to={`${url}/overview`}>
          <h2>Overview</h2>
        </NavLink>
        <NavLink className='TournamentDetailNavigationBar__Link' activeClassName='is-active' to={`${url}/bracket`}>
          <h2>Bracket</h2>
        </NavLink>
        <NavLink className='TournamentDetailNavigationBar__Link' activeClassName='is-active' to={`${url}/teams`}>
          <h2>Teams</h2>
        </NavLink>
        <NavLink className='TournamentDetailNavigationBar__Link' activeClassName='is-active' to={`${url}/rules`}>
          <h2>Rules</h2>
        </NavLink>
        {isCreator ?
          <div>
            <NavLink className='TournamentDetailNavigationBar__Link' activeClassName='is-active' to={`${url}/notification`}>
              <h2>Notification ({pendingTeamIds.length})</h2>
            </NavLink>
          </div>
          :
          null
        }
        </div>
      <Switch>
        <Route path={`${path}/overview`}>
          <TournamentDetailOverview {...data} />
        </Route>
        <Route path={`${path}/bracket`}>
          <TournamentDetailBracket
            teamIds={teams}
            bracketSeedsIds={bracketSeeds}
            dateSeeds={dateSeeds}
            slotSize={slotSize}
            status={status}
            winnerId={winner}
          />
        </Route>
        <Route path={`${path}/teams`}>
          <TournamentDetailTeams teamIds={teams} />
        </Route>
        <Route path={`${path}/rules`}>
          <TournamentDetailRules rules={rules} />
        </Route>
        <Route path={`${path}/notification`}>
          <TournamentDetailNotification
            tournamentId={tournamentId}
            pendingTeamIds={pendingTeamIds}
            isCreator={isCreator}
          />
        </Route>
      </Switch>
    </div>
  )
}

export default TournamentDetailNavigationBar
