import React, {useEffect, useState} from 'react'
import './TournamentCard.css'
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';

const TournamentCard = ({name, description, image}) => {

  const [rawDescription, setRawDescription] = useState("");

  useEffect(() => {
    let tempText = '';
    const doSomething = () => {
      const htmlBody = JSON.parse(description).blocks;
      htmlBody.map((eachBody) => {
        tempText += eachBody.text + ' ';
      })
      setRawDescription(tempText);
    }
    doSomething();

  }, []);


  return (
    <Card className='tournamentCard'>
      <CardMedia
        className='tournamentImage'
        image= {image}
      />
      <CardContent>
        <div className='tournamentName'>
          <h3>{name}</h3>
        </div>
        <div className='tournamentDescription'>
          <h4>{rawDescription.substr(0,50) + '...'}</h4>
        </div>
      </CardContent>
    </Card>
  )
}

export default TournamentCard
