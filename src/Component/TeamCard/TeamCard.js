import React, {useEffect, useState} from 'react'
import './TeamCard.css'
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';

const TeamCard = ({ name, description, image }) => {

  const [rawDescription, setRawDescription] = useState("");

  useEffect(() => {
    let tempText = '';
    const doSomething = () => {
      const htmlBody = JSON.parse(description).blocks;
      htmlBody.map((eachBody) => {
        tempText += eachBody.text + ' ';
      })
      setRawDescription(tempText);
    }

    if(description) {
      doSomething()
    }
  }, []);


  return (
    <Card className='teamCard'>
      <CardMedia
        className='teamImage'
        image={image}
        title={name}
      />
      <CardContent>
        <div className='teamName'>
          <h3>{name}</h3>
        </div>
        {description ? 
          <div className='teamDescription'>
            <h4>{rawDescription.substr(0,50) + '...'}</h4>
          </div>
          :
          null
        }
      </CardContent>
    </Card>
  )
}

export default TeamCard;
