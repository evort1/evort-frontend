import React, {useEffect, useState} from 'react'
import './NewsCard.css'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import moment from 'moment';
import {convertToHTML} from "draft-convert";
import {convertFromRaw} from "draft-js";

import parse from 'html-react-parser';

const NewsCard = ({ title, image, body, date }) => {

  const [rawBody, setRawBody] = useState("");

  useEffect(() => {
    let tempText = '';
    const doSomething = () => {
      const htmlBody = JSON.parse(body).blocks;
      htmlBody.map((eachBody) => {
        tempText += eachBody.text + ' ';
      })
      setRawBody(tempText);
    }
    doSomething();

  }, []);

  return (
    <div>
      <Card className='newsCard'>
        <div className='newsHeader'>
          <h2>{title}</h2>
          <h4>{moment(date).format('LLLL')}</h4>
        </div>
        <img src={image} className='newsImage' alt="" />
        <CardContent>
          <div className='newsBody'>
            <h5>{rawBody.substr(0,300) + '...'}</h5>
          </div>
        </CardContent>
      </Card>
    </div>
  )
}

export default NewsCard;
