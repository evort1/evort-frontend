import React, {useEffect, useState} from 'react'
import {findManyTournamentsByTournamentIds} from '../../../Services/TournamentService'
import {Link} from "react-router-dom";
import TournamentCard from "../../TournamentCard/TournamentCard";
import { isEmpty } from 'lodash';

const TeamDetailTournament = ({tournamentIds}) => {
  const [tournaments, setTournaments] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const result = await findManyTournamentsByTournamentIds(tournamentIds);
      setTournaments(result.data);
    };

    fetchData();
  }, [])

  return (
    <div>
      {isEmpty(tournaments) ?
        <p>No Tournament Joined yet</p>
        :
        <div>
          {tournaments.map ((tournamentData, index) => (
            <Link
              className='tournament__link'
              key={index}
              to={`/tournaments/${tournamentData._id}/overview`}
            >
              <TournamentCard
                name={tournamentData.name}
                description={tournamentData.description}
                image={tournamentData.image}
              />
            </Link>
          ))}
        </div>
      }
    </div>
  );
};
export default TeamDetailTournament;