import React, {useEffect, useState} from 'react'
import './TeamDetailOverview.css';
import {findManyUsersByUserIds} from "../../../Services/UserService";
import Avatar from "@material-ui/core/Avatar";
import {generatedAvatarName} from "../../../Common/generateFromUsername";
import {useStyles} from "./TeamDetailOverviewStyle";
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Tooltip from '@material-ui/core/Tooltip';
import {convertToHTML} from "draft-convert";
import {convertFromRaw} from "draft-js";
import parse from 'html-react-parser';

const TeamDetailOverview = ({data}) => {
  const [members, setMembers] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    const fetchData = async () => {
      const result = await findManyUsersByUserIds(data.candidates);
      setMembers(result.data);
    };

    fetchData();
  }, [])

  return (
    <div>
      <div className="TeamDetailOverview__row">
        <h3>Team Info</h3>
        <p>{parse(convertToHTML(convertFromRaw(JSON.parse(data.description))))}</p>
      </div>
      <div className="TeamDetailOverview__row">
        <h3>Team Members: </h3>
        <div className={classes.root}>
          <GridList className={classes.gridList} cols={members.length}>
            {members.map((member) => (
              <GridListTile>
                <Tooltip title={member.name}>
                  <Avatar alt={member.name} className={classes.teamDetailOverview__avatar}>
                    <span className={classes.teamDetailOverview__avatar__name} >
                      {generatedAvatarName(member.name)}
                    </span>
                  </Avatar>
                </Tooltip>
              </GridListTile>
            ))}
          </GridList>
        </div>
      </div>
    </div>
  );
};
export default TeamDetailOverview;