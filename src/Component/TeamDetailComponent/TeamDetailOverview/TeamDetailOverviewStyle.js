import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    overflow: 'hidden',
  },
  gridList: {
    flexWrap: 'nowrap',
    transform: 'translateZ(0)',
  },
  teamDetailOverview__avatar: () =>({
    color: "black",
    '&:hover': {
      background: "#f5f5f5",
    },
  }),
  teamDetailOverview__avatar__name: {
    fontSize: '20px',
    fontWeight: 'bold',
  },
}));