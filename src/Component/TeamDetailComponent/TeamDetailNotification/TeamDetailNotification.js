import React, {useEffect, useState} from 'react'
import {getCurrentUser} from "../../../Services/AuthService";
import { isEqual, isEmpty } from 'lodash';
import './TeamDetailNotification.css'
import { DataGrid } from '@material-ui/data-grid';
import {useStyles} from "./TeamDetailNotificationStyles";
import Pagination from '@material-ui/lab/Pagination';
import PaginationItem from '@material-ui/lab/PaginationItem';
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";

import {findManyUsersByUserIds} from "../../../Services/UserService";
import {joinTeam, removeFromPending} from "../../../Services/TeamService";

const TeamDetailNotification = ({teamId, pendingUserIds, isUserJoin}) => {
  const [select, setSelection] = useState([]);
  const [rowData, setRowData] = useState([]);

  const classes = useStyles();
  const history = useHistory();

  useEffect(() => {
    const fetchPendingUsersRowData = async () => {
      const {data: pendingUsersData} = await findManyUsersByUserIds(pendingUserIds);
      let tempRow = [];
      isEmpty(pendingUsersData) ?
        tempRow = []
        :
        pendingUsersData.map((pendingUserData, key)=> {
          tempRow.push({id:key+1, name: pendingUserData.name, email: pendingUserData.email, _id: pendingUserData._id});
        })
      setRowData(tempRow);
    };

    fetchPendingUsersRowData();
  }, [])

  const columns = [
    { field: 'id', width: 100,
      cellClassName: 'super-app-theme--cell',
      headerClassName: 'super-app-theme--header',
      renderHeader: () => (<strong> {'No'} </strong>)
    },
    { field: 'name', width: 250,
      cellClassName: 'super-app-theme--cell',
      headerClassName: 'super-app-theme--header',
      renderHeader: () => (<strong> {'Name'} </strong>)
    },
    { field: 'email', width: 500,
      cellClassName: 'super-app-theme--cell',
      headerClassName: 'super-app-theme--header',
      renderHeader: () => (<strong> {'Email'} </strong>
      ),
    },
  ];

  const CustomPagination = (props) => {
    const { pagination, api } = props;
    return (
      <Pagination
        color="primary"
        variant="outlined"
        shape="rounded"
        size="large"
        page={pagination.page}
        count={pagination.pageCount}
        renderItem={(props2) => <PaginationItem className={classes.pagination1}{...props2} disableRipple />}
        onChange={(event, value) => api.current.setPage(value)}
      />
    );
  }

  const handleAcceptTeam = async () => {
    let selectedUserIds = []
      await Promise.all(
        rowData.map( async (pendingUserData) => {
          select.map( async (selected) => {
            if(pendingUserData.id == selected){
              selectedUserIds.push(pendingUserData._id);
            }
          })
        })
      )
    const {data: joinTeamResponses} = await joinTeam(teamId, selectedUserIds);
    await removeFromPending(teamId, selectedUserIds);

    await joinTeamResponses.map( (eachResponse) => {
      alert(eachResponse.message);
    })

    history.push(`/teams/${teamId}/overview`);
    // eslint-disable-next-line no-restricted-globals
    location.reload();
  }

  const handleRejectTeam = async () => {
    let selectedUserIds = []
    await Promise.all(
      rowData.map( async (pendingUserData) => {
        select.map( async (selected) => {
          if(pendingUserData.id == selected){
            selectedUserIds.push(pendingUserData._id);
          }
        })
      })
    )
    const {data: removeResponses} = await removeFromPending(teamId, selectedUserIds);


    await removeResponses.map( (eachResponse) => {
      alert(eachResponse.message);
    })

    history.push(`/teams/${teamId}/overview`);
    // eslint-disable-next-line no-restricted-globals
    location.reload();
  }

  return (
    <div className='teamDetailNotification'>
      {isUserJoin ?
        <div>
          {isEmpty(rowData) ?
            <h2>There is Currently No Notification</h2>
            :
            <div>
              <div className='teamDetailNotification__header'>
                <h2>Users who's Requesting to Join your team</h2>
              </div>
              <div className='dataGrid__teamDetailNotification'>
                <div style={{ height: 400, width: '80%'}} className={classes.root}>
                  <DataGrid
                    rows={rowData}
                    columns={columns}
                    pageSize={5}
                    checkboxSelection
                    components={{
                    pagination: CustomPagination,
                    }}
                    onSelectionChange={(newSelection) => {
                      setSelection(newSelection.rowIds);
                    }}
                  />
                  <Button
                    className='rejectTeamNotificationButton'
                    variant="contained"
                    color="secondary"
                    onClick={handleRejectTeam}
                  >
                    Reject
                  </Button>
                  <Button
                    className='acceptTeamNotificationButton'
                    variant="contained"
                    color="primary"
                    onClick={handleAcceptTeam}
                  >
                    Accept
                  </Button>
                </div>
              </div>
            </div>
          }
        </div>
      :
        <h2 className='teamNotJoined__text'> This page is only accessible for user who joined the team</h2>
      }
    </div>
  )
}
export default TeamDetailNotification;