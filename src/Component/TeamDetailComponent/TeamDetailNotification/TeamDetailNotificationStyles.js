import { makeStyles } from '@material-ui/core/styles';

const customCheckbox = (theme) => {
  return {
    '& .MuiCheckbox-root svg': {
      width: 16,
      height: 16,
      backgroundColor: 'transparent',
      border: `1px solid ${
        theme.palette.type === 'light' ? '#d9d9d9' : 'rgb(67, 67, 67)'
      }`,
      borderRadius: 2,
    },
    '& .MuiCheckbox-root svg path': {
      display: 'none',
    },
    '& .MuiCheckbox-root.Mui-checked:not(.MuiCheckbox-indeterminate) svg': {
      backgroundColor: '#228B22',
      borderColor: '#228B22',
    },
    '& .MuiCheckbox-root.Mui-checked .MuiIconButton-label:after': {
      position: 'absolute',
      display: 'table',
      border: '2px solid #fff',
      borderTop: 0,
      borderLeft: 0,
      transform: 'rotate(45deg) translate(-50%,-50%)',
      opacity: 1,
      transition: 'all .2s cubic-bezier(.12,.4,.29,1.46) .1s',
      content: '""',
      top: '50%',
      left: '39%',
      width: 5.71428571,
      height: 9.14285714,
    },
    '& .MuiCheckbox-root.MuiCheckbox-indeterminate .MuiIconButton-label:after': {
      width: 8,
      height: 8,
      backgroundColor: '#228B22',
      transform: 'none',
      top: '39%',
      border: 0,
    },
  }
}

export const useStyles = makeStyles((theme) => ({
  root: {
    '& .super-app-theme--cell': {
      color: 'white',
      fontWeight: 'bold',
      fontSize:'17px'
    },
    '& .super-app-theme--header': {
      color: '#d1cfcf',
      fontSize:'20px'
    },
    '& .MuiPagination-root' : {
      color: 'white'
    },
    backgroundColor:'#0A1128',
    height: '400',
    width: '50%',

    ...customCheckbox(theme),
  },
  pagination1: {
    backgroundColor: "#d0d3f5",
  }


}));