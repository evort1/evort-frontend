import React, {useEffect, useState} from 'react'
import './TeamDetailNavigationBar.css';
import {
  Switch,
  Route,
  NavLink,
  useRouteMatch
} from "react-router-dom";
import TeamDetailOverview from '../TeamDetailComponent/TeamDetailOverview/TeamDetailOverview';
import TeamDetailTournament from "../TeamDetailComponent/TeamDetailTournaments/TeamDetailTournament";
import TeamDetailNotification from "../TeamDetailComponent/TeamDetailNotification/TeamDetailNotification";
import {findManyUsersByUserIds} from "../../Services/UserService";
import { isEqual, isEmpty } from 'lodash';
import {getCurrentUser} from "../../Services/AuthService";

const TeamDetailNavigationBar = ( {data} ) => {
  const [isUserJoin, setIsUserJoin] = useState(false);

  let { url, path } = useRouteMatch();
  const {tournaments: joinedTournamentIds, candidates: candidateIds, pendingUser: pendingUsersIds} = data;
  const user = getCurrentUser();

  useEffect(() => {
    const isUserInsideTeam = async () => {
      candidateIds.map( (candidateId) => {
        if(isEqual(candidateId, user?.data._id)) {
          setIsUserJoin(true);
        }
      })
    };

    isUserInsideTeam();
  }, [])

  return (
    <div className='TeamDetailNavigationBar'>
      <div className='TeamDetailNavigationBar__Links'>
        <NavLink className='TeamDetailNavigationBar__Link' activeClassName='is-active' to={`${url}/overview`}>
          <h2>Overview</h2>
        </NavLink>
        <NavLink className='TeamDetailNavigationBar__Link' activeClassName='is-active' to={`${url}/tournaments`}>
          <h2>Tournaments</h2>
        </NavLink>
        {isUserJoin ?
          <NavLink className='TeamDetailNavigationBar__Link' activeClassName='is-active' to={`${url}/notification`}>
            <h2>Notification ({pendingUsersIds.length})</h2>
          </NavLink>
          :
          null
        }
      </div>
      <Switch>
        <Route path={`${path}/overview`}>
          <TeamDetailOverview data={data} />
        </Route>
        <Route path={`${path}/tournaments`}>
          <TeamDetailTournament tournamentIds={joinedTournamentIds}/>
        </Route>
        <Route path={`${path}/notification`}>
          <TeamDetailNotification
            teamId={data._id}
            pendingUserIds={pendingUsersIds}
            isUserJoin={isUserJoin}
          />
        </Route>
      </Switch>
    </div>
  );
};

export default TeamDetailNavigationBar;