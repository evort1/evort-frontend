import React, { useState, useEffect } from 'react'
import './App.css'
import MainPage from './Page/MainPage/MainPage'
import TournamentDetailPage from './Page/TournamentDetailPage/TournamentDetailPage';
import TeamDetailPage from './Page/TeamDetailPage/TeamDetailPage'
import CreateTournamentPage from './Page/CreateTournamentPage/CreateTournamentPage'
import CreateTeamPage from './Page/CreateTeamPage/CreateTeamPage';
import NoMatchPage from './Page/NoMatchPage/NoMatchPage'
import Register from './Page/FormPage/RegisterPage'
import LoginPage from './Page/FormPage/LoginPage'
import Tournament from "./Component/Tournament/Tournament";
import Team from "./Component/Team/Team";
import TopNavigationBar from './Component/TopNavigationBar/TopNavigationBar';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import { getCurrentUser } from './Services/AuthService';
import PrivateRoute from './Component/PrivateRoute';
import ProfilePage from './Page/ProfilePage/ProfilePage';
import NewsDetailPage from './Page/NewsDetailPage/NewsDetailPage';
import { isEqual } from 'lodash';
import News from "./Component/News/News";
import CreateNewsPage from "./Page/CreateNewsPage/CreateNewsPage";
import LeaderboardPage from './Page/LeaderboardPage/LeaderboardPage';

const App = () => {
  const [AuthStatus, setAuthStatus] = useState(false);
  const [user, setUser] = useState(getCurrentUser());
  const [adminAuthority, setAdminAuthority] = useState(false);

  useEffect(() => {
    if (user) {
      setAuthStatus(true)
    }
    if(user && isEqual(user?.data.email,'admin@evort' )) {
      setAdminAuthority(true);
    }
  },[])

  useEffect(() => {
    setUser(getCurrentUser());
  },[AuthStatus])

  return (
    <div className="app">
      <Router>
        <TopNavigationBar
          user={user}
          setUser={setUser}
          setAuthStatus={setAuthStatus}
          AuthStatus={AuthStatus}
        />
        <div className='app--page'>
          <Switch>
            <Route
              path={`/tournaments/:tournamentId`}
              component={() => <TournamentDetailPage adminAuthority={adminAuthority}/>}
            />
            <Route
              path={'/teams/:teamId'}
              component={() => <TeamDetailPage adminAuthority={adminAuthority}/>}
            />
            <Route
              path={`/news/:newsId`}
              component={() => <NewsDetailPage adminAuthority={adminAuthority}/>}
            />
            <PrivateRoute exact path='/createTournament' component={CreateTournamentPage} />
            <Route exact path="/tournaments">
              <Tournament />
            </Route>
            <PrivateRoute exact path='/createTeam' component={CreateTeamPage} />
            <Route exact path="/createNews">
              <CreateNewsPage />
            </Route>
            <Route exact path="/teams">
              <Team />
            </Route>
            <Route path="/leaderboard">
              <LeaderboardPage />
            </Route>
            <Route exact path="/news">
              <News adminAuthority={adminAuthority}/>
            </Route>
            <Route path="/signup">
              <Register />
            </Route>
            <Route path="/login">
              <LoginPage
                setAuthStatus={setAuthStatus}
              />
            </Route>
            <Route exact path='/users/:id' component={ProfilePage} />
            <Route exact path="/">
              <MainPage />
            </Route>
            <Route path="*">
              <NoMatchPage />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
