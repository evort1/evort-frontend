import React, { useState } from 'react';
import {useStyles} from "./CreateNewsPageStyles";
import "./CreateNewsPage.css";
import CustomTextField from "../../Component/CustomMaterialUI/CustomTextField";
import Button from "@material-ui/core/Button";
import Tooltip from "@material-ui/core/Tooltip";
import Fade from "@material-ui/core/Fade";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import CardMedia from "@material-ui/core/CardMedia";
import {useFormik} from "formik";
import {createNews} from "../../Services/NewsService";
import { useHistory } from "react-router-dom";
import {createMuiTheme, MuiThemeProvider} from "@material-ui/core/styles";
import MUIRichTextEditor from "mui-rte";
import {convertToRaw} from "draft-js";

const CreateNewsPage = () => {

  const defaultTheme = createMuiTheme()

  Object.assign(defaultTheme, {
    overrides: {
      MUIRichTextEditor: {
        root: {
          color: '#90E0EF',
          marginTop: 20,
          width: "65%"
        },
        toolbar: {
          borderTop: "1px solid #90E0EF",
          backgroundColor: "#2b3969"
        },
        editor: {
          borderBottom: "1px solid #90E0EF",
          backgroundColor: "#2b3969",
          padding: 5,
          height: "200px",
          maxHeight: "200px",
          overflow: "auto",
        },
        placeHolder: {
          color: "#90E0EF",
          padding: 5
        },
      }
    }
  })

  const history = useHistory();
  const classes = useStyles();
  const [newsImage, setNewsImage] = useState(null);

  const handleOnSubmit = async (values) => {
    let bodyFormData = new FormData();
    bodyFormData.set("title", values.title);
    bodyFormData.set("body", values.body);
    bodyFormData.append("image", values.image);

    const {message, status, id:newsId} = await createNews(bodyFormData);
    alert(message);

    if(status === 201){
      history.replace({ pathname: `/news/${newsId}` });
    }
  };

  const formik = useFormik({
    initialValues: {
      title: '',
      body: '',
      image: ''
    },
    onSubmit: values => {
      handleOnSubmit(values);
    },
  });

  const imageHandleOnChange = (event) => {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setNewsImage(reader.result);
    }
    setNewsImage(reader.result);

    formik.setFieldValue("image",file);
  }

  const onChangeTextEditor = (event, field) => {
    const content = JSON.stringify(convertToRaw(event.getCurrentContent()))

    formik.setFieldValue(field, content);
  }


  return (
    <div className='createNewsPage'>
      <form className={classes.form}>
          <h2 className='createNewsPage__title'> Create News Page</h2>
        <div className='createNewsForm__mainLayout'>
          <div className='createNewsForm__row'>
            <CustomTextField
              name='title'
              className="title"
              label="Title"
              variant="outlined"
              onChange={formik.handleChange}
              value={formik.values.title}
            />
          </div>
          <div className='createNewsForm__row'>
            <div className='createNewsForm__uploadImage'>
              <div className='createNewsForm__uploadImageAndTooltip'>
                <Button
                  variant="outlined"
                  color="primary"
                  component="label"
                >
                  Upload News Image
                  <input
                    accept="image/*"
                    name="image"
                    type="file"
                    style={{ display: "none" }}
                    onChange={(e) => imageHandleOnChange(e)}
                  />
                </Button>
                <div>
                  <Tooltip
                    TransitionComponent={Fade}
                    TransitionProps={{ timeout: 800 }}
                    placement="right"
                    title={<h2>Recommended image size : 320*180px</h2>}
                  >
                    <HelpOutlineIcon fontSize="medium" color='primary'/>
                  </Tooltip>
                </div>
              </div>
              {
                newsImage ?
                  <CardMedia
                    className='createNewsForm__uploadImage__image'
                    image={newsImage}
                  />
                  :
                  null
              }
            </div>
          </div>
          <div className='createNewsForm__row'>
            <MuiThemeProvider theme={defaultTheme}>
              <MUIRichTextEditor
                controls={["bold", "italic","underline", 'numberList' , 'bulletList' ]}
                label="Type News Description Here"
                defaultValue={formik.initialValues.description}
                onChange={(e) => onChangeTextEditor(e, 'body')}
              />
            </MuiThemeProvider>
          </div>
        </div>
        <div className= 'submitButton'>
          <Button
            variant="outlined"
            color="primary"
            type="submit"
            onClick={formik.handleSubmit}
            size="small"
          >
            Submit
          </Button>
        </div>
      </form>
    </div>
  );
};
export default CreateNewsPage;