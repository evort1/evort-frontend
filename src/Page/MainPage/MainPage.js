import React from 'react'
import './MainPage.css'
import CarouselLayout from '../../Component/CarouselLayout/CarouselLayout'
import TopThree from '../../Component/TopThree/TopThree';

const MainPage = () => {

  return (
    <div className='mainPage'>
      <CarouselLayout />
      <TopThree game="DefenceOfTheAncient2" />
      <TopThree game="MobileLegendBangBang" />
    </div>
  )
}

export default MainPage
