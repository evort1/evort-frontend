import React, { useEffect, useState, Fragment } from 'react'
import { useParams } from 'react-router';
import { useFetch } from "../../Common/UseFetch";
import Common from "../../Common/Common";
import TournamentDetailNavigationBar from '../../Component/TournamentDetailNavigationBar/TournamentDetailNavigationBar'
import './TournamentDetailPage.css';
import Button from "@material-ui/core/Button";
import './TournamentDetailPage.css';
import { getCurrentUser } from "../../Services/AuthService";
import {findManyTeamByTeamIds, findOneTeamById, findOneTeamByName} from "../../Services/TeamService";
import { getTournamentDataById, requestJoinTournament } from "../../Services/TournamentService";
import { isEmpty, isEqual } from 'lodash';
import { findUserById } from '../../Services/UserService';
import EditTournamentModal from '../../Modal/EditTournamentModal/EditTournamentModal';
import EditTournamentStatusModal from '../../Modal/EditTournamentStatusModal/EditTournamentStatusModal';
import LeaveTournamentModal from "../../Modal/LeaveTournamentModal/LeaveTournamentModal";
import formatter from "../../Common/converterIntToCurrencyIDR";
import JoinTournamentModal from "../../Modal/JoinTournamentModal/JoinTournamentModal";
import DeleteTournamentModal from "../../Modal/DeleteTournamentModal/DeleteTournamentModal";
import JoinTeamPendingUserModal from "../../Modal/JoinTeamPendingUserModal/JoinTeamPendingUserModal";
import { ColorButtonGrey } from "../../Common/ColorButtonGrey"
import { useStyles } from "./TournamentDetailPageStyles";
import JoinTournamentPendingTeamModal from "../../Modal/JoinTournamentPendingTeamModal/JoinTournamentPendingTeamModal";

const {
  baseUrl
} = Common;

const TournamentDetailPage = ({adminAuthority}) => {
  const user = getCurrentUser();
  const { tournamentId } = useParams();
  const classes = useStyles();

  const [handleJoinTournamentModal, setHandleJoinTournamentModal] = useState(false);
  const [teamChosen, setTeamChosen] = useState('');
  const [teamsData, setTeamsData] = useState([]);
  const [openEditTournament, setOpenEditTournament] = useState(false);
  const [openEditTournamentStatus, setOpenEditTournamentStatus] = useState(false);
  const [tournamentImage, setTournamentImage] = useState(null);
  const [handleLeaveTournamentModal, setHandleLeaveTournamentModal] = useState(false);
  const [handleDeleteTournamentModal, setHandleDeleteTournamentModal] = useState(false);
  const [isJoined, setIsJoined] = useState(false);
  const [userJoinedTeamInformation, setUserJoinedTeamInformation] = useState({});
  const [isPendingTeam, setIsPendingTeam] = useState(false);
  const [openPendingTeamModalOpen, setOpenPendingTeamModalOpen] = useState(false);

  const {
    fetchedData: tournamentData, callReFetch
  } = useFetch(`${baseUrl}/tournaments/${tournamentId}`);


  useEffect(() => {
    const newTournamentImage = tournamentData ? tournamentData.image : null;
    setTournamentImage(newTournamentImage);
  }, [tournamentData]);

  useEffect(() => {
    const getTeamData = async () => {
      const userData = await findUserById(user.data._id);
      const { data: teamsDataFromService} = await findManyTeamByTeamIds(userData?.data.teams);
      setTeamsData(teamsDataFromService);
    };

    const getUsersTeamJoinedTournament = async () => {
      let joinedTeamIdByUser = '';
      const { data: userData} = await findUserById(user.data._id);
      await Promise.all(
        userData.teams.map( async (userTeamId) => {
          const { data: userTeams} = await findOneTeamById(userTeamId)
          userTeams.tournaments.map((teamTournamentId) => {
            if(isEqual(teamTournamentId, tournamentId)) {
              setIsJoined(true);
              joinedTeamIdByUser = userTeamId;
            }
          })
        })
      )

      if(!isEmpty(joinedTeamIdByUser)) {
        const {data : tempTeamInformation} = await findOneTeamById(joinedTeamIdByUser);
        setUserJoinedTeamInformation(tempTeamInformation);
      }
    };

    const isTeamInPendingJoinStatus = async () => {
      const {data: tempTournamentData} = await getTournamentDataById(tournamentId);
      await Promise.all(
        tempTournamentData.pendingTeam.map(async (pendingTeamId) => {
          const {data: tempTeamData} = await findOneTeamById(pendingTeamId);
          tempTeamData.candidates.map(async (candidateId) => {
            if(isEqual(candidateId, user?.data._id)) {
              setIsPendingTeam(true);
            }
          })
        })
      )
    };

    if(user) {
      getTeamData();
      getUsersTeamJoinedTournament();
      isTeamInPendingJoinStatus();
    }
  },[])

  const handleOnClickJoinTournamentModal = () => {
    setHandleJoinTournamentModal(true);
  };

  const handleOnClickDeleteTournamentModal = () => {
    setHandleDeleteTournamentModal(true);
  }

  const handleOnCloseDeleteTournamentModal = () => {
    setHandleDeleteTournamentModal(false);
  }

  const handleOnCloseJoinTournamentModal = () => {
    setHandleJoinTournamentModal(false);
  };

  const handleOnChangeRadioButton = (event) => {
    setTeamChosen(event.target.value);
  };

  const handleClickOpenEditTournamentModal = () => {
    setOpenEditTournament(true);
  };

  const handleClickCloseEditTournamentModal = () => {
    // eslint-disable-next-line no-restricted-globals
    location.reload();
    setOpenEditTournament(false);
  };

  const handleClickOpenEditTournamentStatusModal = () => {
    setOpenEditTournamentStatus(true);
  };

  const handleCloseEditTournamentStatusModal = () => {
    setOpenEditTournamentStatus(false);
  };

  const getUpdatedTournamentStatus = () => {
    if (tournamentData?.status === 'upcoming') return 'ongoing';
    if (tournamentData?.status === 'ongoing') return 'completed';
  }

  const getUpdatedTournamentStatusButton = () => {
    if (tournamentData?.status === 'upcoming') return 'Start Tournament';
    if (tournamentData?.status === 'ongoing') return 'End Tournament';
  }

  const handleOnClickJoinTournament = async () => {
    const {data : selectedTeamData} = await findOneTeamByName(teamChosen);

    const { data : joinTournamentInformation } = await requestJoinTournament(tournamentId, selectedTeamData._id);
    alert(joinTournamentInformation.message);
    handleOnCloseJoinTournamentModal();
    // eslint-disable-next-line no-restricted-globals
    location.reload();
  };

  const handleOnClickLeaveTournamentModal = () => {
    setHandleLeaveTournamentModal(true);
  };

  const handleOnPendingTeamModalOpen = () => {
    setOpenPendingTeamModalOpen(true);
  }

  const handleCloseTournamentPendingTeamModal = () => {
    setOpenPendingTeamModalOpen(false);
  }

  return (
    <div className='tournamentDetailPage'>
      {tournamentData ?
        <Fragment>
          <div className='tournamentDetailPage__row'>
          <div>
            <h1 className="tournamentDetailPage__title">{tournamentData.name}</h1>
            <h2>( {tournamentData.status} )</h2>
            <img
              className="tournamentDetailPage__tournamentImage"
              src={tournamentImage}
            />
          </div>
          <div className='tournamentDetailPage__prizePool'>
            <img
              className="tournamentDetailPage__trophyImage"
              src="https://freepngimg.com/thumb/trophy/75846-trophy-cup-icon-free-download-image.png"
            />
            <h2>Prize Pool : {formatter.format(tournamentData.prizePool)}</h2>
          </div>
          </div>
          <div className='deleteTournament__button'>
            {adminAuthority ?
              <div>
                <Button
                  className='deleteTournamentButton'
                  color='secondary'
                  variant='outlined'
                  onClick={handleOnClickDeleteTournamentModal}
                >
                  Delete Tournament
                </Button>
                <DeleteTournamentModal
                  tournamentId = { tournamentId }
                  handleDeleteTournamentModal={handleDeleteTournamentModal}
                  handleOnCloseDeleteTournamentModal={handleOnCloseDeleteTournamentModal}
                />
              </div>
              : null
            }
          </div>
          {tournamentData.status === 'upcoming' ?
            <Fragment>
              {user && isJoined ?
                <div>
                  <Button
                    className='leaveTournamentButton'
                    color='secondary'
                    variant='outlined'
                    onClick={handleOnClickLeaveTournamentModal}
                  >
                    Leave Tournament
                  </Button>
                  <LeaveTournamentModal
                    tournamentId= { tournamentId }
                    userJoinedTeamInformation = { userJoinedTeamInformation }
                    handleLeaveTournamentModal= { handleLeaveTournamentModal }
                    setHandleLeaveTournamentModal= { setHandleLeaveTournamentModal }
                  />
                </div>
              :
                <div>
                  {isPendingTeam ?
                    <div>
                      <ColorButtonGrey
                        variant="contained"
                        color="primary"
                        className='TournamentDetailPage__disabledJoinButton'
                        onClick={handleOnPendingTeamModalOpen}
                      >
                        Join Team
                      </ColorButtonGrey>
                      <JoinTournamentPendingTeamModal
                        openPendingTeamModalOpen={openPendingTeamModalOpen}
                        handleCloseTournamentPendingTeamModal={handleCloseTournamentPendingTeamModal}
                      />
                    </div>
                    :
                  <div>
                    {adminAuthority ?
                      null
                      :
                      <div>
                        <Button
                          className='joinTournamentButton'
                          color='primary'
                          variant='outlined'
                          onClick={handleOnClickJoinTournamentModal}
                        >
                          Join Tournament
                        </Button>
                        <JoinTournamentModal
                          teamChosen = {teamChosen}
                          handleOnChangeRadioButton = {handleOnChangeRadioButton}
                          teamsData = {teamsData}
                          handleOnClickJoinTournament = {handleOnClickJoinTournament}
                          handleJoinTournamentModal = {handleJoinTournamentModal}
                          handleOnCloseJoinTournamentModal = {handleOnCloseJoinTournamentModal}
                          user = {user}
                        />
                      </div>
                     }
                  </div>
                  }
                </div>
              }
            </Fragment>
            : null
          }
          {tournamentData.createdBy === user?.data._id ?
            <Fragment>
              {tournamentData.status === 'upcoming' ?
                <Button 
                  className='editButton' 
                  variant="outlined" 
                  color="primary" 
                  onClick={handleClickOpenEditTournamentModal}
                >
                  Edit Tournament
                </Button>
                : null
              }
              {tournamentData.status !== 'completed' ?
                <div className='tournamentDetailPage__updateTournamentState'>
                  <Button 
                    className="updateButton" 
                    variant="outlined" 
                    color="primary" 
                    onClick={handleClickOpenEditTournamentStatusModal}
                  >
                    {getUpdatedTournamentStatusButton()}
                  </Button>
                </div>
                :
                <h2 className='updateTournamentStateText'>Tournament Has Ended</h2>
              }
            </Fragment>
            : null
          }
          <EditTournamentModal
            open={openEditTournament}
            setOpen={setOpenEditTournament}
            data={tournamentData}
            callReFetch={callReFetch}
            tournamentImage={tournamentImage}
            setTournamentImage={setTournamentImage}
            handleClose={handleClickCloseEditTournamentModal}
          />
          <EditTournamentStatusModal
            getUpdateTournamentStatus={getUpdatedTournamentStatus}
            open={openEditTournamentStatus}
            setOpen={setOpenEditTournamentStatus}
            handleClose={handleCloseEditTournamentStatusModal}
            data={tournamentData}
          />
          <TournamentDetailNavigationBar data={tournamentData} />
        </Fragment>
        : null
      }
    </div>
  )
}

export default TournamentDetailPage
