import { makeStyles } from '@material-ui/core/styles';
import { createMuiTheme } from '@material-ui/core/styles'

export const useStyles = makeStyles((theme) => ({
  button: {
    "&:disabled": {
      backgroundColor: "grey"
    }
  }
}));

export const theme = createMuiTheme({
  palette: {
    action: {
      disabledBackground: 'set color of background here',
      disabled: 'set color of text here'
    }
  }
});