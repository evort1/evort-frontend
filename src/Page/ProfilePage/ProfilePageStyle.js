import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  root: {
    background: '#161d4d',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  profilePage__avatar: props =>({
    color: "black",
    background: props.color,
    width: theme.spacing(20),
    height: theme.spacing(20),
  }),
  profilePage__avatar__name: {
    fontSize: '20px',
    fontWeight: 'bold',
  },
}));