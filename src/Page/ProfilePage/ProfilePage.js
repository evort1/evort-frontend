import React, { useState, useEffect, Fragment } from 'react';
import './ProfilePage.css';
import { findManyTeamByTeamIds } from "../../Services/TeamService";
import { findManyTournamentsByTournamentIds } from "../../Services/TournamentService";
import { findUserById } from '../../Services/UserService';
import { Link } from "react-router-dom";
import {useStyles} from "./ProfilePageStyle";
import TeamCard from "../../Component/TeamCard/TeamCard";
import TournamentCard from "../../Component/TournamentCard/TournamentCard";
import { useFetch } from "../../Common/UseFetch";
import Common from "../../Common/Common";
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {generatedAvatarColor, generatedAvatarName} from "../../Common/generateFromUsername";
import Avatar from "@material-ui/core/Avatar";
import { useParams } from 'react-router';

const {
  baseUrl
} = Common;

const ProfilePage = () => {
  const {id: userIdParams}= useParams();
  const [userInformation, setUserInformation] = useState(null);
  const props = userInformation ? { color: generatedAvatarColor(userInformation?.data.username) } : { color: "white" } ;
  const classes = useStyles(props);
  const [joinedTeamsData, setJoinedTeamsData] = useState(null);
  const [joinedTournamentsData, setJoinedTournamentData] = useState(null);
  const { fetchedData: createdTournamentsData } = useFetch(`${baseUrl}/tournaments/users/${userInformation?._id}`);

  useEffect(() => {
    const fetchData = async () => {
      const userData = await findUserById(userIdParams);
      setUserInformation(userData);

      const { data: userTeams } = await findManyTeamByTeamIds(userData?.data.teams);
      setJoinedTeamsData(userTeams);

      const { data: userTournaments } = await findManyTournamentsByTournamentIds(userData?.data.tournaments);
      setJoinedTournamentData(userTournaments);
    };

    fetchData();
  }, [])

  return (
    <div className='profilePage'>
      {userInformation ?
        <div>
          <div className='profilePageName'>
            <h1>Welcome to {userInformation.data.username}'s Profile Page</h1>
          </div>
          <div className='profilePagePicture'>
            <Avatar alt={userInformation.data.username} className={classes.profilePage__avatar}>
              <span className={classes.profilePage__avatar__name} >
                {generatedAvatarName(userInformation.data.username)}
              </span>
            </Avatar>
          </div>
          <div className='profilePageBorder'>
            <div className="profilePageData">
              <h3>Dota 2 Points</h3>
              <p>{userInformation.data.points.DefenceOfTheAncient2} Points</p>
            </div>
            <div className="profilePageData">
              <h3>Mobile Legends Points</h3>
              <p>{userInformation.data.points.MobileLegendBangBang} Points</p>
            </div>
          </div>
          <div className="profilePageBorder">
            <div className="profilePageData ">
              <h3>Email</h3>
              <p>{userInformation.data.email}</p>
            </div>
          </div>
          <div className='profilePage__Accordion'>
            <Accordion className={classes.root}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography className={classes.heading}>{userInformation.data.username}'s Created Tournament</Typography>
              </AccordionSummary>
              <AccordionDetails>
                {createdTournamentsData ?
                  <Fragment>
                    {createdTournamentsData.length !== 0 ?
                      <div className='tournament__list'>
                        {createdTournamentsData.map((tournamentData, index) => (
                          <Link
                            className='tournament__link'
                            key={index}
                            to={`/tournaments/${tournamentData._id}/overview`}
                          >
                            <TournamentCard
                              name={tournamentData.name}
                              description={tournamentData.description}
                              image={tournamentData.image}
                            />
                          </Link>
                        ))}
                      </div>
                      :
                      <p>{userInformation.data.username} haven't created any tournament</p>
                    }
                  </Fragment>
                  : null
                }
              </AccordionDetails>
            </Accordion>
          <Accordion className={classes.root}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.heading}>{userInformation.data.username}'s Joined Tournament</Typography>
            </AccordionSummary>
              <AccordionDetails>
                {joinedTournamentsData ?
                  <Fragment>
                    {joinedTournamentsData.length !== 0 ?
                      <div className='tournament__list'>
                        {joinedTournamentsData.map((tournamentData, index) => (
                          <Link
                            className='tournament__link'
                            key={index}
                            to={`/tournaments/${tournamentData._id}/overview`}
                          >
                            <TournamentCard
                              name={tournamentData.name}
                              description={tournamentData.description}
                              image={tournamentData.image}
                            />
                          </Link>
                        ))}
                      </div>
                      :
                      <p>{userInformation.data.username} haven't joined any tournament</p>
                    }
                  </Fragment>
                  : null
                }
              </AccordionDetails>
            </Accordion>
            <Accordion className={classes.root}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel2a-content"
                id="panel2a-header"
              >
                <Typography className={classes.heading}>{userInformation.data.username}'s Joined Teams</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <Typography>
                  {joinedTeamsData ?
                    <Fragment>
                      {joinedTeamsData.length !== 0 ?
                        <div className='team__list'>
                          {joinedTeamsData.map((teamData, index) => (
                            <Link
                              className='team__link'
                              key={index}
                              to={`/teams/${teamData._id}/overview`}
                            >
                              <TeamCard
                                name={teamData.name}
                                description={teamData.description}
                                image={teamData.image}
                                points={teamData.points}
                              />
                            </Link>
                          ))}
                        </div>
                        :
                        <p>{userInformation.data.username} haven't joined any team</p>
                      }
                    </Fragment>
                    : null
                  }
                </Typography>
              </AccordionDetails>
            </Accordion>
          </div>
        </div>
        :
        null
      }
    </div>
  )
}

export default ProfilePage;