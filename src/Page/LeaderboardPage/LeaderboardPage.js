import React, { useState, useEffect } from 'react';
import './LeaderboardPage.css'
import useStyles from './LeaderboardPageStyle';
import { useFetch } from "../../Common/UseFetch";
import Common from "../../Common/Common";
import { DataGrid } from '@material-ui/data-grid';

const { baseUrl } = Common;

const columns = [
  {
    field: 'id', width: 100,
    cellClassName: 'super-app-theme--cell',
    headerClassName: 'super-app-theme--header',
    renderHeader: () => (<strong> {'No'} </strong>),
  },
  {
    field: 'name', width: 250,
    cellClassName: 'super-app-theme--cell',
    headerClassName: 'super-app-theme--header',
    renderHeader: () => (<strong> {'Username'} </strong>),
  },
  {
    field: 'email', width: 250,
    cellClassName: 'super-app-theme--cell',
    headerClassName: 'super-app-theme--header',
    renderHeader: () => (<strong> {'Email'} </strong>),
  },
  {
    field: 'points', width: 250,
    cellClassName: 'super-app-theme--cell',
    headerClassName: 'super-app-theme--header',
    renderHeader: () => (<strong> {'User Points'} </strong>),
  }
];

const LeaderboardPage = () => {
  const classes = useStyles();
  const [displayData, setDisplayData] = useState([]);
  const [gameKey, setGameKey] = useState("DefenceOfTheAncient2");

  const {
    fetchedData: usersData, callReFetch
  } = useFetch(`${baseUrl}/user/leaderboard/10/${gameKey}`);

  const handleGameChange = (game) => {
    setGameKey(game);
    callReFetch();
  }

  useEffect(() => {
    let tempDisplaydata = [];
    const createData = (id, name, points, email) => {
      return { id, name, points, email };
    }

    if (usersData) {
      usersData.map((data, id) => {
        return tempDisplaydata.push(createData(id + 1, data.name, data.points[gameKey], data.email));
      })
      setDisplayData(tempDisplaydata);
    }
  }, [usersData]);

  return (
    <div className="LeaderboardPage">
      <h1 className="LeaderboardPage__title">Leaderboard Page</h1>
      <div className="LeaderboardPage__row ">
        <h2 
          onClick={() => handleGameChange("DefenceOfTheAncient2") } 
          className={`left LeaderboardPage__button__text ${gameKey === "DefenceOfTheAncient2" ? "LeaderboardPage__button__text__selected": null}`}
        >
          Dota 2
        </h2>
        <h2 
          className={`right LeaderboardPage__button__text ${gameKey === "MobileLegendBangBang" ? "LeaderboardPage__button__text__selected": null}`}   
          onClick={() => handleGameChange("MobileLegendBangBang") }
        >
          Mobile Legends
        </h2>
      </div>
      {displayData.length !== 0 ?
        <div style={{ height: 600, width: 900 }} className={classes.root}>
          <DataGrid
            rows={displayData}
            columns={columns}
            pageSize={5}
            hideFooterPagination
            hideFooter
            disableColumnMenu
          />
        </div>
        :
        null
      }
    </div>
  )
}

export default LeaderboardPage;
