import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    '& .super-app-theme--cell': {
      color: '#FFFFFF',
      fontWeight: 'bold',
      fontSize:'17px',
    },
    '& .super-app-theme--header': {
      color: '#e3e3e3',
      fontSize:'20px'
    },
    backgroundColor:'#0A1128',
    margin: "2%"
  },
});

export default useStyles;