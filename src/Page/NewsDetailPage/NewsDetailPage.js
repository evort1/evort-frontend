import React, {useState} from 'react';
import './newsDetailPage.css';
import { useParams } from 'react-router';
import { useFetch } from "../../Common/UseFetch";
import Common from "../../Common/Common";
import moment from "moment";
import Button from "@material-ui/core/Button";
import DeleteNewsModal from "../../Modal/DeleteNewsModal/DeleteNewsModal";
import {convertToHTML} from "draft-convert";
import {convertFromRaw} from "draft-js";
import parse from 'html-react-parser';
const { baseUrl } = Common;

const NewsDetailPage = ({adminAuthority}) => {
  const { newsId } = useParams();
  const { fetchedData: data } = useFetch(`${baseUrl}/news/${newsId}`);
  const [openDeleteNewsModal, setOpenDeleteNewsModal] = useState(false);

  const handleOnClickDeleteTeamButton = () => {
    setOpenDeleteNewsModal(true);
  }
  const handleOnCloseDeleteNewsModal = () => {
    setOpenDeleteNewsModal(false);
  }

  return (
    <div className='newsDetailPage'>
      {data ?
        <div>
          <div>
            {adminAuthority ?
              <div className='teamDetailNews__deleteButton'>
                <Button
                  className='deleteNewsButton'
                  color='secondary'
                  variant='outlined'
                  onClick={handleOnClickDeleteTeamButton}
                >
                  Delete News
                </Button>
                <DeleteNewsModal
                  newsId={newsId}
                  openDeleteNewsModal={openDeleteNewsModal}
                  handleOnCloseDeleteNewsModal={handleOnCloseDeleteNewsModal}
                />
              </div>
              : null
            }
          </div>
          <div>
            <h1 className='newsDetailPage__title'>{data.title}</h1>
            <h4 className='newsDetailPage__dateCreated'>{moment(data.created_at).format('LLLL')}</h4>
            <img className='newsDetailPage__image' src={data.image} alt=""/>
            <p className='newsDetailPage__body' >{parse(convertToHTML(convertFromRaw(JSON.parse(data.body))))}</p>
          </div>
        </div>
        : null
      }
    </div>
  )
}

export default NewsDetailPage
