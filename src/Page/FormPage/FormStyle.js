import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  form: {
    '& .MuiInputBase-root': {
      color: '#90E0EF'
    }
  }
});