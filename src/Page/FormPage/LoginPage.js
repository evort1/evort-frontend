import React, { useEffect } from 'react'
import CustomTextField from '../../Component/CustomMaterialUI/CustomTextField';
import './Form.css'
import Button from '@material-ui/core/Button';
import {
  Link
} from "react-router-dom";
import { useFormik } from 'formik';
import { authUser } from '../../Services/AuthService';
import { useLocation, useHistory } from "react-router-dom";
import { useStyles } from './FormStyle';

const LoginPage = ({ setAuthStatus }) => {
  const history = useHistory();
  const location = useLocation();
  const classes = useStyles();

  const { from, message } = location.state || { from: { pathname: "/" }, message: null };

  useEffect(() => {
    if (location.state) {
      alert(message);
    }
  }, [])

  const loginHandling = async (values) => {
    const { status, message } = await authUser(values);
    alert(message);
    if (status === 200) {
      setAuthStatus(true);
      history.replace(from);
    }
  }

  const { handleSubmit, getFieldProps } = useFormik({
    initialValues: {
      email: '',
      password: ''
    },
    onSubmit: (values) => loginHandling(values)
  });

  return (
    <div className='loginPage'>
      <div className='formPage__form'>
        <h1>Login your Evort Account</h1>
        <div className='loginPage__createNewAccount'>
          <p>Need an Account?</p>
          <Link id="createAccount" to="/signup">
            <p>Create one</p>
          </Link>
        </div>
        <form noValidate autoComplete="off" onSubmit={handleSubmit} className={classes.form}>
          <fieldset>
            <div className='formPage__form--row'>
              <div className='formPage__form__component'>
                <CustomTextField
                  name="email"
                  label="Email"
                  size="small"
                  variant="outlined"
                  {...getFieldProps("email")}
                />
              </div>
            </div>
            <div className='formPage__form--row'>
              <div className='formPage__form__component'>
                <CustomTextField
                  name="password"
                  label="Password"
                  size="small"
                  variant="outlined"
                  type='password'
                  {...getFieldProps("password")}
                />
              </div>
            </div>
            <div className='formPage__form__component'>
              <Button variant="outlined" color="primary" type='submit'>
                Log in
              </Button>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  )
}

export default LoginPage
