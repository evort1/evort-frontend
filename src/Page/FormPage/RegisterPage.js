import React from 'react'
import CustomTextField from '../../Component/CustomMaterialUI/CustomTextField';
import './Form.css'
import Button from '@material-ui/core/Button';
import {
  Link, useHistory
} from "react-router-dom";
import { useFormik } from 'formik';
import { createUser } from '../../Services/AuthService';
import { useStyles } from './FormStyle';

function RegisterPage() {
  const history = useHistory();
  const classes = useStyles();

  const registerHandling = async(values) => {
    const {status, message} = await createUser(values);
    alert(message);
    if(status === 201){
      history.replace({ pathname: "/login" });
    }
  }

  const { handleSubmit, getFieldProps } = useFormik({
    initialValues: {
      name: '',
      username: '',
      email: '',
      password: ''
    },
    onSubmit: (values) => registerHandling(values)
  });

  return (
    <div className='registerPage'>
      <div className='formPage__form'>
        <h1>Create your Evort Account</h1>
        <div className='registerPage__login'>
          <p>Already have an account?</p>
          <Link id="logIn" to="/login">
            <p>Log In</p>
          </Link>
        </div>
        <form noValidate autoComplete="off" onSubmit={handleSubmit} className={classes.form}>
          <fieldset>
            <div className='formPage__form--row'>
              <div className='formPage__form__component'>
                <CustomTextField
                  name='name'
                  label="Name"
                  size="small"
                  variant="outlined"
                  {...getFieldProps('name')}
                />
              </div>
              <div className='formPage__form__component'>
                <CustomTextField
                  name='username'
                  label="Username"
                  size="small"
                  variant="outlined"
                  {...getFieldProps('username')}
                />
              </div>
            </div>
            <div className='formPage__form--row'>
              <div className='formPage__form__component'>
                <CustomTextField
                  name='email'
                  label="Email"
                  size="small"
                  variant="outlined"
                  {...getFieldProps('email')}
                />
              </div>
              <div className='formPage__form__component'>
                <CustomTextField
                  name='password'
                  label="Password"
                  size="small"
                  variant="outlined"
                  type='password'
                  {...getFieldProps('password')}
                />
              </div>
            </div>
            <div className='formPage__form__component'>
              <Button variant="outlined" color="primary" type='submit'>
                Register
            </Button>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  )
}

export default RegisterPage
