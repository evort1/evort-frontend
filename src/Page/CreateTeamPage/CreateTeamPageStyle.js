import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  form: {
    '& .MuiTextField-root': {
      marginTop: theme.spacing(1),
      marginRight: theme.spacing(2),
      marginBottom: theme.spacing(1),
      width: '20ch',
      display: "block"
    },
    '& .MuiInputBase-root': {
      color: '#90E0EF'
    }
  },
  description: {
    '& .MuiInputBase-root': {
      width: '400%'
    }
  }
}));