import React, { useState } from 'react';
import { useFormik } from 'formik';
import './CreateTeamPage.css';
import Button from '@material-ui/core/Button';
import CustomTextField from "../../Component/CustomMaterialUI/CustomTextField";
import { useStyles } from "./CreateTeamPageStyle";
import CardMedia from '@material-ui/core/CardMedia';
import { createTeam } from '../../Services/TeamService';
import { joinTeam, findOneTeamByName } from '../../Services/TeamService';
import { getCurrentUser } from "../../Services/AuthService";
import { useHistory } from "react-router-dom";
import Tooltip from "@material-ui/core/Tooltip";
import Fade from "@material-ui/core/Fade";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import {createMuiTheme, MuiThemeProvider} from "@material-ui/core/styles";
import MUIRichTextEditor from "mui-rte";
import {convertToRaw} from "draft-js";

const CreateTeamPage = () => {

  const defaultTheme = createMuiTheme()

  Object.assign(defaultTheme, {
    overrides: {
      MUIRichTextEditor: {
        root: {
          color: '#90E0EF',
          marginTop: 20,
          width: "65%"
        },
        toolbar: {
          borderTop: "1px solid #90E0EF",
          backgroundColor: "#2b3969"
        },
        editor: {
          borderBottom: "1px solid #90E0EF",
          backgroundColor: "#2b3969",
          padding: 5,
          height: "200px",
          maxHeight: "200px",
          overflow: "auto",
        },
        placeHolder: {
          color: "#90E0EF",
          padding: 5
        },
      }
    }
  })

  const classes = useStyles();
  const user = getCurrentUser();

  const [teamImage, setTeamImage] = useState(null);
  const history = useHistory();

  const handleOnSubmit = async (values) => {
    let bodyFormData = new FormData();
    bodyFormData.set("name", values.name);
    bodyFormData.set("description", values.description);
    bodyFormData.append("image", values.teamImage);

    const {message, status, id: teamId} = await createTeam(bodyFormData);
    alert(message)
    if(status === 201){
      const teamData = await findOneTeamByName(bodyFormData.get("name"));
      await joinTeam(teamData.data._id, [user?.data._id]);
      history.replace({ pathname: `/teams/${teamId}/overview` });
    }
  };

  const formik = useFormik({
    initialValues: {
      name: '',
      description: '',
      teamImage: ''
    },
    onSubmit: values => {
      handleOnSubmit(values);
    },
  });

  const imageHandleOnChange = (event) => {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setTeamImage(reader.result);
    }
    setTeamImage(reader.result);

    formik.setFieldValue("teamImage",file);
  }

  const onChangeTextEditor = (event, field) => {
    const content = JSON.stringify(convertToRaw(event.getCurrentContent()))
    formik.setFieldValue(field, content);
  }

  return (
    <div className='createTeamForm'>
      <form className={classes.form}>
        <h3 className='createTeamForm__title'>Create Team</h3>
        <div className='createTeamForm__mainLayout'>
          <div className='createTeamForm__row'>
            <CustomTextField
              name='name'
              className="name"
              label="Team name"
              variant="outlined"
              onChange={formik.handleChange}
              value={formik.values.teamName}
            />
          </div>
          <div className='createTeamForm__row'>
            <div className='createTeamForm__uploadImage'>
              <div className='createTeamForm__uploadImageAndTooltip'>
                <Button
                  variant="outlined"
                  color="primary"
                  component="label"
                >
                  Upload Team Image
                  <input
                    accept="image/*"
                    name="image"
                    type="file"
                    style={{ display: "none" }}
                    onChange={(e) => imageHandleOnChange(e)}
                  />
                </Button>
                <div>
                  <Tooltip
                    TransitionComponent={Fade}
                    TransitionProps={{ timeout: 800 }}
                    placement="right"
                    title={<h2>Recommended image size : 320*180px</h2>}
                  >
                    <HelpOutlineIcon fontSize="medium" color='primary'/>
                  </Tooltip>
                </div>
              </div>
                {
                  teamImage ?
                    <CardMedia
                      className='createTeamForm__uploadImage__image'
                      image={teamImage}
                    />
                    :
                    null
                }
            </div>
          </div>
          <div className='createTeamForm__row'>
            <MuiThemeProvider theme={defaultTheme}>
              <MUIRichTextEditor
                controls={["bold", "italic","underline", 'numberList' , 'bulletList' ]}
                label="Type Team Description Here"
                defaultValue={formik.initialValues.description}
                onChange={(e) => onChangeTextEditor(e, 'description')}
              />
            </MuiThemeProvider>
            </div>
        </div>
        <div className='submitButton'>
          <Button
            variant="outlined"
            color="primary"
            type="submit"
            onClick={formik.handleSubmit}
            size="small"
          >
            Submit
          </Button>
        </div>
      </form>
    </div>
  );
};
export default CreateTeamPage;
