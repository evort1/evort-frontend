import React, { useState } from 'react';
import { useFormik } from 'formik';
import './CreateTournamentPage.css';
import { createTournament } from "../../Services/TournamentService";
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import { useStyles,  } from "./CreateTournamentPageStyle";
import CardMedia from '@material-ui/core/CardMedia';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import {
	useHistory
} from "react-router-dom";
import { getCurrentUser } from "../../Services/AuthService";
import { slotSizeChoices, gameChoices } from "../../Common/constanta";
import CustomTextField from "../../Component/CustomMaterialUI/CustomTextField";
import CustomDatePicker from "../../Component/CustomMaterialUI/CustomDatePicker";
import CustomTimePicker from "../../Component/CustomMaterialUI/CustomTimePicker";
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import Tooltip from "@material-ui/core/Tooltip";
import Fade from '@material-ui/core/Fade';
import MUIRichTextEditor from 'mui-rte'
import {createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { convertToRaw } from 'draft-js'

const CreateTournamentPage = () => {
	const classes = useStyles();
	const user = getCurrentUser();

	const defaultTheme = createMuiTheme()

	Object.assign(defaultTheme, {
		overrides: {
			MUIRichTextEditor: {
				root: {
					color: '#90E0EF',
					marginTop: 20,
					width: "65%"
				},
				toolbar: {
					borderTop: "1px solid #90E0EF",
					backgroundColor: "#2b3969"
				},
				editor: {
					borderBottom: "1px solid #90E0EF",
					backgroundColor: "#2b3969",
					padding: 5,
					height: "200px",
					maxHeight: "200px",
					overflow: "auto",
				},
				placeHolder: {
					color: "#90E0EF",
					padding: 5
				},
			}
		}
	})


	const [tournamentImage, setTournamentImage] = useState(null);
	const history = useHistory();

	const handleOnSubmit = async (values) => {
		let bodyFormData = new FormData();

		bodyFormData.set("name", values.name);
		bodyFormData.set("description", values.description);
		bodyFormData.set("createdBy", user.data._id);
		bodyFormData.set("startDate", values.startDate);
		bodyFormData.set("endDate", values.endDate);
		bodyFormData.set("entryFee", values.entryFee);
		bodyFormData.set("prizePool", values.prizePool);
		bodyFormData.set("slotSize", values.slotSize);
		bodyFormData.set("game", values.game);
		bodyFormData.set("rules", values.rules);
		bodyFormData.append("image", values.tournamentImage);

		const { message, status: statusCreateTournamentStatus, id:tournamentId } = await createTournament(bodyFormData);
		alert(message);

		if (statusCreateTournamentStatus === 201) {
			history.replace({ pathname: `/tournaments/${tournamentId}/overview` });
		}
	}

	const formik = useFormik({
		initialValues: {
			name: '',
			description: '',
			rules: '',
			slotSize: '',
			game:'',
			tournamentImage: '',
			createdBy: '',
			prizePool: 0,
			entryFee: 0,
			startDate: null,
			endDate: null
		},
		onSubmit: values => {
			handleOnSubmit(values);
		},
	});

	const dateHandleChange = (date, value) => {
		formik.setFieldValue(date, value);
	}

	const imageHandleOnChange = (event) => {
		const file = event.target.files[0];
		const reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onloadend = () => {
			setTournamentImage(reader.result);
		}

		formik.setFieldValue('tournamentImage', file);
	}

	const onChangeTextEditor = (event, field) => {
		const content = JSON.stringify(convertToRaw(event.getCurrentContent()))

		formik.setFieldValue(field, content);
	}

	return (
		<div className='createTournamentForm'>
			<form className={classes.form}>
				<h3 className='createTournamentForm__title'>Create Tournament Form</h3>
				<div className='createTournamentForm__mainLayout'>
					<div className='createTournamentForm__row'>
						<CustomTextField
							name='name'
							className="name"
							label="Tournament name"
							variant="outlined"
							onChange={formik.handleChange}
							value={formik.values.tournamentName}
						/>
					</div>
					<div className='createTournamentForm__row'>
					<CustomTextField
						name='prizePool'
						label="Prize Pool"
						variant="outlined"
						type="number"
						onChange={formik.handleChange}
						value={formik.values.prizePool}
						InputProps={{
							startAdornment: <InputAdornment position="start">Rp</InputAdornment>,
						}}
					/>
					<CustomTextField
						name='entryFee'
						label="Entry Fee"
						variant="outlined"
						type="number"
						onChange={formik.handleChange}
						value={formik.values.entryFee}
						InputProps={{
							startAdornment: <InputAdornment position="start">Rp</InputAdornment>,
						}}
					/>
					<CustomTextField
						name='slotSize'
						select
						label="Slot Size"
						variant="outlined"
						value={formik.values.slotSize}
						onChange={formik.handleChange}
					>
						{slotSizeChoices.map((option) => (
							<MenuItem key={option.value} value={option.value}>
								{option.label}
							</MenuItem>
						))}
					</CustomTextField>
					<CustomTextField
						name='game'
						select
						label="Game Name"
						variant="outlined"
						value={formik.values.game}
						onChange={formik.handleChange}
					>
						{gameChoices.map((option) => (
							<MenuItem key={option.value} value={option.value}>
								{option.name}
							</MenuItem>
						))}
					</CustomTextField>
					</div>	
					<MuiPickersUtilsProvider utils={DateFnsUtils}>
						<div className='createTournamentForm__row'>
							<CustomDatePicker
								inputVariant="outlined"
								format="MM/dd/yyyy"
								label="Start Date"
								onChange={(e) => dateHandleChange("startDate", e)}
								value={formik.values.startDate}
							/>
							<CustomTimePicker
								inputVariant="outlined"
								label="Start Time"
								onChange={(e) => dateHandleChange("startDate", e)}
								value={formik.values.startDate}
							/>
							<CustomDatePicker
								inputVariant="outlined"
								format="MM/dd/yyyy"
								label="End Date"
								onChange={(e) => dateHandleChange("endDate", e)}
								value={formik.values.endDate}
							/>
							<CustomTimePicker
								inputVariant="outlined"
								label="End Time"
								onChange={(e) => dateHandleChange("endDate", e)}
								value={formik.values.endDate}
							/>
						</div>
					</MuiPickersUtilsProvider>
					<div className='createTournamentForm__row'>
						<div className='createTournamentForm__uploadImage'>
							<div className='createTournamentForm__uploadImageAndTooltip'>
								<Button
									variant="outlined"
									component="label"
									color="primary"
								>
									Upload Tournament Image
								<input
										accept="image/*"
										name="image"
										type="file"
										style={{ display: "none" }}
										onChange={(e) => imageHandleOnChange(e)}
									/>
								</Button>
								<div>
									<Tooltip
										TransitionComponent={Fade}
										TransitionProps={{ timeout: 800 }}
										placement="right"
										title={<h2>Recommended Image size : 320*180px</h2>}
									>
									<HelpOutlineIcon fontSize="medium" color='primary'/>
									</Tooltip>
								</div>
							</div>
							{
								tournamentImage ?
									<CardMedia
										className='createTournamentForm__uploadImage__image'
										image={tournamentImage}
									/>
									:
									null
							}
						</div>
					</div>
					<div className='createTournamentForm__row'>
						<MuiThemeProvider theme={defaultTheme}>
							<MUIRichTextEditor
								controls={["bold", "italic","underline", 'numberList' , 'bulletList' ]}
								label="Type Tournament Rules Here"
								defaultValue={formik.initialValues.rules}
								onChange={(e) => onChangeTextEditor(e, 'rules')}
							/>
						</MuiThemeProvider>
					</div>
					<div className='createTournamentForm__row'>
						<MuiThemeProvider theme={defaultTheme}>
							<MUIRichTextEditor
								controls={["bold", "italic","underline", 'numberList' , 'bulletList' ]}
								label="Type Tournament Description Here"
								defaultValue={formik.initialValues.description}
								onChange={(e) => onChangeTextEditor(e, 'description')}
							/>
						</MuiThemeProvider>
					</div>
				</div>
				<div className='submitButton'>
					<Button
						className={classes.button}
						variant="outlined"
						color="primary"
						type="submit"
						onClick={formik.handleSubmit}
						size="small"
					>
						Submit
					</Button>
				</div>
			</form>
		</div>
	);
};
export default CreateTournamentPage
