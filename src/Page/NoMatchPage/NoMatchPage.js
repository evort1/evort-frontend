import React from 'react'

const NoMatchPage = () => {
  return (
    <div>
      <h1>No Page found</h1>
    </div>
  )
}

export default NoMatchPage
