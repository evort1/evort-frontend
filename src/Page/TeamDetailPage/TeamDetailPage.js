import React, {Fragment, useEffect, useState} from 'react'
import { useParams } from 'react-router';
import { useFetch } from "../../Common/UseFetch";
import Common from "../../Common/Common";
import Button from '@material-ui/core/Button';
import {findOneTeamById, requestJoinTeam, leaveTeam} from '../../Services/TeamService';
import {getCurrentUser} from "../../Services/AuthService";
import './TeamDetailPage.css';
import TeamDetailNavigationBar from "../../Component/TeamDetailNavigationBar/TeamDetailNavigationBar";
import JoinTeamModal from "../../Modal/JoinTeamModal/JoinTeamModal";
import LeaveTeamModal from "../../Modal/LeaveTeamModal/LeaveTeamModal";
import { isEqual} from 'lodash';
import { useStyles } from "./TeamDetailPageStyles";

import JoinTeamPendingUserModal  from '../../Modal/JoinTeamPendingUserModal/JoinTeamPendingUserModal'
import DeleteTeamModal from "../../Modal/DeleteTeamModal/DeleteTeamModal";
import { ColorButtonGrey } from "../../Common/ColorButtonGrey"

const {
  baseUrl
} = Common;

const TeamDetailPage = ({adminAuthority}) => {
  const { teamId } = useParams();
  const [openJoinTeamModal, setOpenJoinTeamModal] = useState(false);
  const [openLeaveTeamModal, setOpenLeaveTeamModal] = useState(false);
  const [isJoined, setIsJoined] = useState(false);
  const [teamImage, setTeamImage] = useState(false);
  const [openDeleteTeamModal, setOpenDeleteTeamModal] = useState(false);
  const [isPendingUser, setIsPendingUser] = useState(false);
  const [openJoinTeamPendingUserModal, setOpenJoinTeamPendingUserModal] = useState(false);

  const user = getCurrentUser();
  const classes = useStyles();

  const {
    fetchedData: data
  } = useFetch(`${baseUrl}/teams/${teamId}`);

  useEffect(() => {
    const newTeamImage = data ? data.image : null;
    setTeamImage(newTeamImage);
  }, [data]);

  useEffect(() => {
    const getUserIsJoined = async () => {
      const {data : useEffectData} = await findOneTeamById(teamId);
      useEffectData.candidates.map((candidate) => {
        if(candidate === user?.data._id){
          setIsJoined(true);
        }
      })
    }

    const isUserInPendingJoinStatus = async () => {
      const {data : teamData} = await findOneTeamById(teamId);
      teamData.pendingUser.map((pendingUserId) => {
        if(isEqual(pendingUserId, user?.data._id)){
          setIsPendingUser(true);
        }
      })
    }
    isUserInPendingJoinStatus();
    getUserIsJoined();
  }, []);

  const handleJoinTeamOnClick = () => {
    setOpenJoinTeamModal(true);
  };

  const handleCloseTeamModal = () => {
    setOpenJoinTeamModal(false);
  }

  const handleModalOnclickJoinTeam = async () => {
    const {data: requestJoinTeamInformation} = await requestJoinTeam(teamId, user.data._id);
    alert(requestJoinTeamInformation.message);
    setOpenJoinTeamModal(false);
    // eslint-disable-next-line no-restricted-globals
    location.reload();
  };

  const handleLeaveTeamOnClick = () => {
    setOpenLeaveTeamModal(true);
  };

  const handleOnClickDeleteTeamButton = () => {
    setOpenDeleteTeamModal(true);
  };

  const handleOnCloseDeleteTeamModal = () => {
    setOpenDeleteTeamModal(false);
  }

  const handleCloseLeaveTeamModal = () => {
    setOpenLeaveTeamModal(false);
  }

  const handleModalOnclickLeaveTeam = async () => {
    const { data : joinTeamInformation} = await leaveTeam(teamId, user.data._id);
    alert(joinTeamInformation.message);
    setOpenLeaveTeamModal(false);
    // eslint-disable-next-line no-restricted-globals
    location.reload();
  };

  const handleOnPendingUserModalOpen = () => {
    setOpenJoinTeamPendingUserModal(true);
  }


  const handleCloseTeamPendingUserModal = () => {
    setOpenJoinTeamPendingUserModal(false);
  }
  return (
    <div className='teamDetailPage'>
      {data ?
        <Fragment>
          <h1 className="teamDetailPage__title">{data.name}</h1>
          <div className="teamDetailPage__row">
            <img
              className="teamDetailPage__teamImage"
              src={teamImage}
            />
          </div>
          <div className="teamDetailPage__deleteButton">
            {adminAuthority ?
              <div>
                <Button
                  className='deleteTeamButton'
                  color='secondary'
                  variant='outlined'
                  onClick={handleOnClickDeleteTeamButton}
                >
                  Delete Team
                </Button>
                <DeleteTeamModal
                  teamId={teamId}
                  openDeleteTeamModal={openDeleteTeamModal}
                  handleOnCloseDeleteTeamModal={handleOnCloseDeleteTeamModal}
                />
              </div>
              :null
            }
          </div>
          {user && isJoined ?
            <div>
              <Button className='leaveTeamButton' variant="outlined" color="secondary" onClick={handleLeaveTeamOnClick}>
                Leave Team
              </Button>
              <LeaveTeamModal
                openLeaveTeamModal={openLeaveTeamModal}
                handleCloseLeaveTeamModal={handleCloseLeaveTeamModal}
                handleModalOnclickLeaveTeam={handleModalOnclickLeaveTeam}
              />
            </div>
            :
            <div>
              {isPendingUser ?
                <div>
                  <ColorButtonGrey
                    variant="contained"
                    color="primary"
                    className='TeamDetailPage__disabledJoinButton'
                    onClick={handleOnPendingUserModalOpen}>
                    Join Team
                  </ColorButtonGrey>
                  <JoinTeamPendingUserModal
                    openJoinTeamPendingUserModal={openJoinTeamPendingUserModal}
                    handleCloseTeamPendingUserModal={handleCloseTeamPendingUserModal}
                  />
                </div>
              :
                <div>
                  {adminAuthority ?
                    null
                  :
                    <div>
                      <Button  className={classes.button} variant="outlined" color="primary" onClick={handleJoinTeamOnClick}>
                        Join Team
                      </Button>
                      <JoinTeamModal
                        openJoinTeamModal={openJoinTeamModal}
                        handleCloseTeamModal={handleCloseTeamModal}
                        handleModalOnclickJoinTeam={handleModalOnclickJoinTeam}
                        user={user}
                      />
                    </div>
                  }
                </div>
              }

            </div>
          }
          <TeamDetailNavigationBar data={data}/>
        </Fragment>
      : null
      }
    </div>
  )
}

export default TeamDetailPage
