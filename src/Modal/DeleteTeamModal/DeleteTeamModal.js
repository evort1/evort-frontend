import Modal from "@material-ui/core/Modal";
import React from "react";
import {useStyles} from "./DeleteTeamModalStyles";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import Button from "@material-ui/core/Button";
import {useHistory} from "react-router-dom";
import {deleteTeam} from "../../Services/TeamService";


const DeleteTeamModal = ({teamId, openDeleteTeamModal, handleOnCloseDeleteTeamModal}) => {
  const classes = useStyles();
  const history = useHistory();


  const handleOnDeleteTeamModal = async () => {
    const {data: message}= await deleteTeam(teamId);
    alert(message);
    history.push('/tournaments');
  }

  const renderDeleteTeamModal = () => {
    return (
      <div className={classes.paper}>
        <h2>Are you sure you want to delete the Team ? </h2>
        <FormControl component="fieldset">
          <FormLabel className={classes.deleteTeamLabel}>
            This change can't be undo. You can never restore the team.
          </FormLabel>
        </FormControl>
        <div className='selectionButton'>
          <Button
            variant="contained"
            color="secondary"
            className='cancelButton'
            onClick={handleOnCloseDeleteTeamModal}
          >
            Cancel
          </Button>
          <Button variant="contained" color="primary" onClick={handleOnDeleteTeamModal}>
            Yes
          </Button>
        </div>
      </div>
    )
  }


  return (
    <Modal
      className={classes.modal}
      open={openDeleteTeamModal}
      onClose={handleOnCloseDeleteTeamModal}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      {renderDeleteTeamModal()}
    </Modal>
  )
}

export default DeleteTeamModal;