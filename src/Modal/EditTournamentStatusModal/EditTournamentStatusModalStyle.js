import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  editTournamentStatus: {
    backgroundColor: "#0A1128",
    padding: theme.spacing(4, 4, 3),
    color: 'white'
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
}));
