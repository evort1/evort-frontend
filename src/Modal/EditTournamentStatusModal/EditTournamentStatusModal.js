import React from 'react';
import { updateTournamentStatus, updateTournament } from '../../Services/TournamentService';
import { updateUserPoints } from "../../Services/UserService";
import { useParams } from 'react-router';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { useStyles } from './EditTournamentStatusModalStyle';
import { shuffleTeam } from "../../Common/RoundsGenerator";
import { useHistory } from "react-router-dom";

const EditTournamentStatusModal = ({ getUpdateTournamentStatus, open, handleClose, data }) => {
  const { tournamentId } = useParams();
  const classes = useStyles();
  const history = useHistory();

  const getBracketSeeds = ({ teams }) => {
    let shuffledTeam = [];
    let dateMatch = [];
    let bodyFormData = new FormData();
    let additionalSlot;
    let slotSize;

    if (teams.length <= 4) {
      additionalSlot = 2;
      slotSize = 4;
      bodyFormData.set("slotSize", slotSize);
    } else {
      additionalSlot = 6;
      slotSize = 8;
      bodyFormData.set("slotSize", slotSize);
    }

    shuffleTeam(teams);

    for (let index = 0; index < slotSize; index++) {
      teams[index] !== undefined ? shuffledTeam.push(teams[index]) : shuffledTeam.push('null');
      if (index % 2) dateMatch.push(new Date().toDateString());
    }

    for (let index = 0; index < additionalSlot; index++) {
      shuffledTeam.push('null');
      if (index % 2) dateMatch.push(new Date().toDateString());
    }

    shuffledTeam.map(team => {
      return bodyFormData.append("bracketSeeds", team);
    })
    dateMatch.map(date => {
      return bodyFormData.append("dateSeeds", date);
    })

    return bodyFormData;
  }

  const handleClickUpdateTournamentStatus = async () => {
    const nextTournamentStatus = getUpdateTournamentStatus();

    if (nextTournamentStatus == "completed" ) {
      if(data.winner){
        const { status: updateTournamentStatusesStatus } = await updateTournamentStatus(nextTournamentStatus, tournamentId);
        const { status: updateTeamsPointsStatus } = await updateUserPoints(data.teams, data.winner, data.game);

        if(updateTournamentStatusesStatus === 200 && updateTeamsPointsStatus === 200){
          handleClose();
          history.replace({ pathname: `/tournaments/${tournamentId}/overview` });
          // eslint-disable-next-line no-restricted-globals
          location.reload();
        }
      }else{
        alert("You should assign the winner of the tournament " +
        "in tournamentBracketModal at the bracket section and click the edit bracket button" +
        ", before change the status of the tournament to end!");
        handleClose();
      }
    }else{
      const bracketSeedsBodyForm = getBracketSeeds(data);
      const { status: updateTournamentBracket } = await updateTournament(bracketSeedsBodyForm, tournamentId);
      const { status: updateTournamentStatusesStatus } = await updateTournamentStatus(nextTournamentStatus, tournamentId);
      
      if (updateTournamentStatusesStatus === 200 && updateTournamentBracket === 200) {
        handleClose();
        history.replace({ pathname: `/tournaments/${tournamentId}/bracket` });
        // eslint-disable-next-line no-restricted-globals
        location.reload();
      }
    } 
  }

  return (
    <div>
      <Modal
        className={classes.modal}
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <div className={classes.editTournamentStatus}>
          <h2 id="simple-modal-title">{"Are you sure change the tournament status changed to " + getUpdateTournamentStatus() + "?"}</h2>
          <FormControl component="fieldset">
            <FormLabel className={classes.editTournamentStatus} >This change can't be undo</FormLabel>
          </FormControl>
          <div className='selectionButton'>
            <Button variant="contained" color="secondary" className='cancelButton' onClick={handleClose}>
              Cancel
            </Button>
            <Button variant="contained" color="primary" onClick={handleClickUpdateTournamentStatus}>
              YES
            </Button>
          </div>
        </div>
      </Modal>
    </div>
  )
}

export default EditTournamentStatusModal;
