import Button from "@material-ui/core/Button";
import Modal from "@material-ui/core/Modal";
import React from "react";
import {useStyles} from "./JoinTeamModalStyles";
import { isEmpty } from 'lodash';
import { useHistory } from "react-router-dom";

const JoinTeamModal = ({ openJoinTeamModal, handleCloseTeamModal, handleModalOnclickJoinTeam, user }) => {
  const classes = useStyles();
  const history = useHistory();

  const renderModalDetail = () => {
    return (
      <div>
        {isEmpty(user) ?
          <div className={classes.paper}>
            <h2 id="simple-modal-title">You haven't logged in yet</h2>
            <div className='selectionButton'>
              <Button variant="contained"  color="secondary" className='cancelButton' onClick={handleCloseTeamModal}>
                Cancel
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={() => history.push('/login')}>
                Login
              </Button>
            </div>
          </div>
          :
          <div>
            <div className={classes.paper}>
              <h2 id="simple-modal-title">
                Are you sure you want to join this team ?
              </h2>
              <div className='selectionButton'>
                <Button variant="contained" color="secondary" onClick={handleCloseTeamModal}>
                  Cancel
                </Button>
                <Button variant="contained" color="primary" onClick={handleModalOnclickJoinTeam}>
                  OK
                </Button>
              </div>
            </div>
          </div>
        }
      </div>
    )
  }

  return (
    <Modal
      className={classes.modal}
      open={openJoinTeamModal}
      onClose={handleCloseTeamModal}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      {renderModalDetail()}
    </Modal>
  )
}

export default JoinTeamModal;