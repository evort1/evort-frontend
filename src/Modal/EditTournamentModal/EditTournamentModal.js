import React from 'react';
import { useParams } from 'react-router';
import './EditTournamentModal.css'
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';
import MenuItem from '@material-ui/core/MenuItem';
import {
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import CardMedia from '@material-ui/core/CardMedia';
import { useStyles } from "./EditTournamentModalStyle";
import { useFormik } from 'formik';
import { slotSizeChoices, gameChoices } from "../../Common/constanta";
import { getCurrentUser } from "../../Services/AuthService";
import { updateTournament } from '../../Services/TournamentService';
import Modal from '@material-ui/core/Modal';
import CustomTextField from '../../Component/CustomMaterialUI/CustomTextField'
import CustomDatePicker from '../../Component/CustomMaterialUI/CustomDatePicker'
import CustomTimePicker from '../../Component/CustomMaterialUI/CustomTimePicker'
import { urlRegex } from '../../Common/urlRegex';
import {createMuiTheme, MuiThemeProvider} from "@material-ui/core/styles";
import MUIRichTextEditor from "mui-rte";
import {convertFromRaw, convertToRaw} from "draft-js";
import {convertToHTML} from "draft-convert";

const EditTournamentModal = ({ open, handleClose, data, callReFetch, tournamentImage, setTournamentImage }) => {
  const defaultTheme = createMuiTheme()

  Object.assign(defaultTheme, {
    overrides: {
      MUIRichTextEditor: {
        root: {
          color: '#90E0EF',
          marginTop: 20,
          width: "65%"
        },
        toolbar: {
          borderTop: "1px solid #90E0EF",
          backgroundColor: "#2b3969"
        },
        editor: {
          borderBottom: "1px solid #90E0EF",
          backgroundColor: "#2b3969",
          padding: 5,
          height: "200px",
          maxHeight: "200px",
          overflow: "auto",
        },
        placeHolder: {
          color: "#90E0EF",
          padding: 5
        },
      }
    }
  })

  const classes = useStyles();
  const user = getCurrentUser();
  const { tournamentId } = useParams();

  const handleOnSubmit = async (values) => {
    let bodyFormData = new FormData();

    bodyFormData.set("name", values.name);
    bodyFormData.set("description", values.description);
    bodyFormData.set("createdBy", user.data._id);
    bodyFormData.set("startDate", values.startDate);
    bodyFormData.set("endDate", values.endDate);
    bodyFormData.set("entryFee", values.entryFee);
    bodyFormData.set("prizePool", values.prizePool);
    bodyFormData.set("url", urlRegex(values.url));
    bodyFormData.set("slotSize", values.slotSize);
		bodyFormData.set("game", values.game);
    bodyFormData.set("rules", values.rules);
    bodyFormData.append("image", values.image);

    const { status: updateTournamentStatus } = await updateTournament(bodyFormData, tournamentId);

    if (updateTournamentStatus === 200) {
      handleClose();
      callReFetch();
    }
  }

  const formik = useFormik({
    initialValues: data,
    onSubmit: values => {
      handleOnSubmit(values);
    },
  });

  const dateHandleChange = (date, value) => {
    formik.setFieldValue(date, value);
  }

  const imageHandleOnChange = (event) => {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setTournamentImage(reader.result);
    }

    formik.setFieldValue("image", file);
  }

  const onChangeTextEditor = (event, field) => {

    const content = JSON.stringify(convertToRaw(event.getCurrentContent()))

    formik.setFieldValue(field, content);
  }


  return (
    <div>
      <Modal
        className={classes.modal}
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <div className={classes.paper}>
          <form className={classes.form}>
          <h2 id="simple-modal-title">Edit Tournament</h2>
          <div className='editTournamentForm__mainLayout'>
              <div className='editTournamentForm__row'>
                <CustomTextField
                  name='name'
                  className="name"
                  label="Tournament name"
                  variant="outlined"
                  onChange={formik.handleChange}
                  value={formik.values.name}
                />
              </div>
              <div className='editTournamentForm__row'>
                <CustomTextField
                  name='prizePool'
                  label="Prize Pool"
                  variant="outlined"
                  type="number"
                  onChange={formik.handleChange}
                  value={formik.values.prizePool}
                  InputProps={{
                    startAdornment: <InputAdornment position="start">Rp</InputAdornment>,
                  }}
                  disabled
                />
                <CustomTextField
                  name='entryFee'
                  label="Entry Fee"
                  variant="outlined"
                  type="number"
                  onChange={formik.handleChange}
                  value={formik.values.entryFee}
                  InputProps={{
                    startAdornment: <InputAdornment position="start">Rp</InputAdornment>,
                  }}
                  disabled
                />
                <CustomTextField
                  name='slotSize'
                  select
                  label="Slot Size"
                  variant="outlined"
                  value={formik.values.slotSize}
                  onChange={formik.handleChange}
                  style={{ width: 150 }}
                >
                  {slotSizeChoices.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </CustomTextField>
                <CustomTextField
                  name='game'
                  select
                  label="Game Name"
                  variant="outlined"
                  value={formik.values.game}
                  onChange={formik.handleChange}
                  disabled
                >
                  {gameChoices.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.value}
                    </MenuItem>
                  ))}
                </CustomTextField>
              </div>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <div className='editTournamentForm__row'>
                  <CustomDatePicker
                    inputVariant="outlined"
                    format="MM/dd/yyyy"
                    label="Start Date"
                    onChange={(e) => dateHandleChange("startDate", e)}
                    value={formik.values.startDate}
                  />
                  <CustomTimePicker
                    inputVariant="outlined"
                    label="Start Time"
                    onChange={(e) => dateHandleChange("startDate", e)}
                    value={formik.values.startDate}
                  />
                </div>
                <div className='editTournamentForm__row'>
                  <CustomDatePicker
                    inputVariant="outlined"
                    format="MM/dd/yyyy"
                    label="End Date"
                    onChange={(e) => dateHandleChange("endDate", e)}
                    value={formik.values.endDate}
                  />
                  <CustomTimePicker
                    inputVariant="outlined"
                    label="End Time"
                    onChange={(e) => dateHandleChange("endDate", e)}
                    value={formik.values.endDate}
                  />
                </div>
                <div className='editTournamentForm__row'>
                  <CustomTextField
                    name='url'
                    className={classes.url}
                    label="Url Video"
                    variant="outlined"
                    onChange={formik.handleChange}
                    value={formik.values.url}
                  />
                </div>
              </MuiPickersUtilsProvider>
              <div className='editTournamentForm__row'>
                <div className='editTournamentForm__uploadImage'>
                  <Button
                    variant="outlined"
                    color="primary"
                    component="label"
                  >
                    Edit Tournament Image
							<input
                      accept="image/*"
                      name="image"
                      type="file"
                      style={{ display: "none" }}
                      onChange={(e) => imageHandleOnChange(e)}
                    />
                  </Button>
                  {
                    tournamentImage ?
                      <CardMedia
                        className='editTournamentForm__uploadImage__image'
                        image={tournamentImage}
                      />
                      :
                      null
                  }
                </div>
              </div>
            <MuiThemeProvider theme={defaultTheme}>
              <MUIRichTextEditor
                controls={["bold", "italic","underline", 'numberList' , 'bulletList' ]}
                label="Type Tournament Description Here"
                defaultValue={formik.initialValues.description}
                onChange={(e) => onChangeTextEditor(e, 'description')}
              />
            </MuiThemeProvider>
            <MuiThemeProvider theme={defaultTheme}>
              <MUIRichTextEditor
                controls={["bold", "italic","underline", 'numberList' , 'bulletList' ]}
                label="Type Tournament Rules Here"
                defaultValue={formik.initialValues.rules}
                onChange={(e) => onChangeTextEditor(e, 'rules')}
              />
            </MuiThemeProvider>
            </div>
          </form>
          <div className='selectionButton'>
            <Button onClick={handleClose} variant="contained" color="secondary">
              Cancel
            </Button>
            <Button onClick={formik.handleSubmit} variant="contained" color="primary">
              Edit
            </Button>
          </div>
        </div>
      </Modal>
    </div>
  )
}

export default EditTournamentModal
