import Modal from "@material-ui/core/Modal";
import React from "react";
import {useStyles} from "./DeleteNewsModalStyles";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import Button from "@material-ui/core/Button";
import {useHistory} from "react-router-dom";
import {deleteNews} from "../../Services/NewsService";

const DeleteNewsModal = ({newsId, openDeleteNewsModal, handleOnCloseDeleteNewsModal}) => {
  const history = useHistory();
  const classes = useStyles();

  const handleOnDeleteNews = async () => {
    const {data: message}= await deleteNews(newsId);
    alert(message);
    history.push('/news');
  }

  const renderDeleteNewsModal = () => {
    return (
      <div className={classes.paper}>
        <h2>Are you sure you want to delete the News ? </h2>
        <FormControl component="fieldset">
          <FormLabel className={classes.deleteNewsLabel}>
            This change can't be undo. You can never restore the news.
          </FormLabel>
        </FormControl>
        <div className='selectionButton'>
          <Button
            variant="contained"
            color="secondary"
            className='cancelButton'
            onClick={handleOnCloseDeleteNewsModal}
          >
            Cancel
          </Button>
          <Button variant="contained" color="primary" onClick={handleOnDeleteNews}>
            Yes
          </Button>
        </div>
      </div>
    )
  };

  return (
    <Modal
      className={classes.modal}
      open={openDeleteNewsModal}
      onClose={handleOnCloseDeleteNewsModal}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      {renderDeleteNewsModal()}
    </Modal>
  )
}

export default DeleteNewsModal;