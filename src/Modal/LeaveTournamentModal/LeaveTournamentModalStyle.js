import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  paper: {
    backgroundColor: "#0A1128",
    padding: theme.spacing(4, 4, 3)
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  availableTeamText: {
    margin: theme.spacing(1,0,0,0),
    color: 'white',
  }
}));