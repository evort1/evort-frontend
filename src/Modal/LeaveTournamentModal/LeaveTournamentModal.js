import Button from "@material-ui/core/Button";
import React from "react";
import { leaveTournament } from "../../Services/TournamentService";
import { getCurrentUser } from "../../Services/AuthService";
import { useStyles } from './LeaveTournamentModalStyle';
import Modal from '@material-ui/core/Modal';

const LeaveTournamentModal = ({
  tournamentId, userJoinedTeamInformation,
  handleLeaveTournamentModal, setHandleLeaveTournamentModal
}) => {
  const user = getCurrentUser();
  const classes = useStyles();

  const handleOnCloseLeaveTournamentModal = () => {
    setHandleLeaveTournamentModal(false);
  };

  const handleOnClickLeaveTournament = async () => {
    const { data: leaveTournamentInformation } = await leaveTournament(tournamentId, userJoinedTeamInformation._id, user.data._id);
    alert(leaveTournamentInformation.message);
    handleOnCloseLeaveTournamentModal();
    // eslint-disable-next-line no-restricted-globals
    location.reload();
  }

  return (
    <Modal
    className={classes.modal}
    open={handleLeaveTournamentModal}
    onClose={handleOnCloseLeaveTournamentModal}
    aria-labelledby="simple-modal-title"
    aria-describedby="simple-modal-description"
    >
      <div className={classes.paper}>
        <h2 id="simple-modal-title"> Are you sure you want to leave with Team {userJoinedTeamInformation.name} ? </h2>
        <div className='selectionButton'>
          <Button variant="contained"  color="secondary" className='cancelButton' onClick={handleOnCloseLeaveTournamentModal}>
            Cancel
          </Button>
          <Button variant="contained" color="primary" onClick={handleOnClickLeaveTournament}>
            OK
          </Button>
        </div>
      </div>
    </Modal>
  )
}

export default LeaveTournamentModal;