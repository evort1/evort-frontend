import Modal from "@material-ui/core/Modal";
import Button from "@material-ui/core/Button";
import React from "react";
import {useStyles} from "./LeaveTeamModalStyles";

const LeaveTeamModal = ({openLeaveTeamModal, handleCloseLeaveTeamModal, handleModalOnclickLeaveTeam }) => {
  const classes = useStyles();

  return (
    <div>
      <Modal
        className={classes.modal}
        open={openLeaveTeamModal}
        onClose={handleCloseLeaveTeamModal}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <div className={classes.paper}>
          <h2 id="simple-modal-title">
            Are you sure you want to Leave this team ?
          </h2>
          <div className='selectionButton'>
            <Button variant="contained" color="secondary" onClick={handleCloseLeaveTeamModal}>
              Cancel
            </Button>
            <Button variant="contained" color="primary"  onClick={handleModalOnclickLeaveTeam}>
              OK
            </Button>
          </div>
        </div>
      </Modal>
    </div>
  )
}

export default LeaveTeamModal;