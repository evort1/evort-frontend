import Modal from "@material-ui/core/Modal";
import React from "react";
import {useStyles} from "./DeleteTournamentModalStyles";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import Button from "@material-ui/core/Button";
import { deleteTournament} from "../../Services/TournamentService";
import {useHistory} from "react-router-dom";

const DeleteTournamentModal = ({tournamentId, handleDeleteTournamentModal, handleOnCloseDeleteTournamentModal }) => {

  const classes = useStyles();
  const history = useHistory();

  const handleDeleteTournament = async () => {
    const {data: message}= await deleteTournament(tournamentId);
    alert(message);
    history.push('/tournaments')
  };


  const renderDeleteTournamentModal = () => {
    return (
      <div className={classes.paper}>
        <h2>Are you sure you want to delete the Tournament ? </h2>
        <FormControl component="fieldset">
          <FormLabel className={classes.deleteTournamentLabel}>
            This change can't be undo. You can never restore the tournament.
          </FormLabel>
        </FormControl>
        <div className='selectionButton'>
          <Button
            variant="contained"
            color="secondary"
            className='cancelButton'
            onClick={handleOnCloseDeleteTournamentModal}
          >
            Cancel
          </Button>
          <Button variant="contained" color="primary" onClick={handleDeleteTournament}>
            Yes
          </Button>
        </div>
      </div>
    )
  }


  return (
    <Modal
      className={classes.modal}
      open={handleDeleteTournamentModal}
      onClose={handleOnCloseDeleteTournamentModal}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      {renderDeleteTournamentModal()}
    </Modal>
  )
}

export default DeleteTournamentModal;