import React from 'react'
import Modal from "@material-ui/core/Modal";
import {useStyles} from "../JoinTeamModal/JoinTeamModalStyles";
import Button from "@material-ui/core/Button";
import './JoinTeamPendingUserModal.css'
const JoinTeamPendingUserModal = ({openJoinTeamPendingUserModal, handleCloseTeamPendingUserModal}) => {
  const classes = useStyles();

  const renderPendingUserModal = () => {
    return (
      <div className={classes.paper}>
        <h2 id="simple-modal-title">
          You've requested to join the team
        </h2>
        <h4 id="simple-modal-title">
          Please wait until the team member accepts your request.
        </h4>
        <div className='selectionButton__pendingUserModal'>
          <Button variant="contained" color="secondary" onClick={handleCloseTeamPendingUserModal}>
            Back
          </Button>
        </div>
      </div>
    )
  }

  return(
    <Modal
      className={classes.modal}
      open={openJoinTeamPendingUserModal}
      onClose={handleCloseTeamPendingUserModal}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      {renderPendingUserModal()}
    </Modal>
  )
}
export default JoinTeamPendingUserModal;