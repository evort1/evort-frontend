import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import NeonBlueRadio from "../../Component/CustomMaterialUI/CustomRadio";
import Button from "@material-ui/core/Button";
import React from "react";
import {useStyles} from "./JoinTournamentModalStyles";
import { isEmpty } from 'lodash';
import Modal from "@material-ui/core/Modal";
import { useHistory } from "react-router-dom";

const JoinTournamentModal = ({
  teamChosen, handleOnChangeRadioButton, teamsData,
  handleOnClickJoinTournament, handleJoinTournamentModal,
  handleOnCloseJoinTournamentModal, user }) => {
  const history = useHistory();
  const classes = useStyles();

  const renderModalDetail = () => {
    return (
      <div>
        {isEmpty(user) ?
          <div className={classes.paper}>
            <h2 id="simple-modal-title">You haven't logged in yet</h2>
            <div className='selectionButton'>
              <Button variant="contained"  color="secondary" className='cancelButton' onClick={handleOnCloseJoinTournamentModal}>
                Cancel
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={() => history.push('/login')}>
                Login
              </Button>
            </div>
          </div>
        :
          <div className='renderModalDetail'>
            <div className={classes.paper}>
              <h2 id="simple-modal-title">Which team are you going to join the tournament with ?</h2>
              <FormControl component="fieldset">
                <FormLabel className={classes.availableTeamText} >Available Team : </FormLabel>
                <RadioGroup aria-label="chooseTeam" name="chooseTeam" value={teamChosen} onChange={handleOnChangeRadioButton}>
                  {isEmpty(teamsData) ?
                    <h3 className='noTeamAvailable'>
                      No Team Available.
                      Please join or create a team.
                    </h3>
                    :
                    teamsData.map((team, index) => (
                      <FormControlLabel key={index} value={`${team.name}`} className={classes.availableTeamText} control={<NeonBlueRadio />} label={`${team.name}`} />
                    ))
                  }
                </RadioGroup>
              </FormControl>
              <div className='selectionButton'>
                <Button variant="contained"  color="secondary" className='cancelButton' onClick={handleOnCloseJoinTournamentModal}>
                  Cancel
                </Button>
                <Button variant="contained" color="primary" onClick={handleOnClickJoinTournament}>
                  OK
                </Button>
              </div>
            </div>
          </div>
        }
      </div>
    )
  }

  return (
    <Modal
      className={classes.modal}
      open={handleJoinTournamentModal}
      onClose={handleOnCloseJoinTournamentModal}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      {renderModalDetail()}
    </Modal>
  )
};

export default JoinTournamentModal;