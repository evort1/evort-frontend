import React from 'react'
import Modal from "@material-ui/core/Modal";
import {useStyles} from "./JoinTournamentPendingTeamModalStyles";
import Button from "@material-ui/core/Button";
import './JoinTournamentPendingTeamModal.css'

const JoinTournamentPendingTeamModal = ({openPendingTeamModalOpen,handleCloseTournamentPendingTeamModal}) => {
  const classes = useStyles();

  const renderPendingTeamModal = () => {
    return (
      <div className={classes.paper}>
        <h2 id="simple-modal-title">
          You've requested to join the tournament
        </h2>
        <h4 id="simple-modal-title">
          Please wait until the Tournament Creator accepts your request.
        </h4>
        <div className='selectionButton__pendingTeamModal'>
          <Button variant="contained" color="secondary" onClick={handleCloseTournamentPendingTeamModal}>
            Back
          </Button>
        </div>
      </div>
    )
  }

  return(
    <Modal
      className={classes.modal}
      open={openPendingTeamModalOpen}
      onClose={handleCloseTournamentPendingTeamModal}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      {renderPendingTeamModal()}
    </Modal>
  )
}
export default JoinTournamentPendingTeamModal;