import React, {useState, useEffect} from 'react'
import './EditTournamentBracketModal.css';
import Modal from '@material-ui/core/Modal';
import Button from "@material-ui/core/Button";
import { Bracket } from 'react-brackets';
import { getBracketSeed } from '../../Common/RoundsGenerator';
import Grid from '@material-ui/core/Grid';
import { useStyles } from './EditTournamentBracketModalStyle';
import { useFormik } from 'formik';
import { updateTournament } from '../../Services/TournamentService';
import { useParams } from 'react-router';
import CustomTextField from "../../Component/CustomMaterialUI/CustomTextField";
import MenuItem from '@material-ui/core/MenuItem';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import CustomDatePicker from "../../Component/CustomMaterialUI/CustomDatePicker";

const EditTournamentBracketModal = ({ 
  open, handleClose, bracketSeeds, 
  disabledFirstMatch, dateSeeds, teamsAutocomplete, 
  teamsData, slotSize, winnerName 
  }) => {
  const classes = useStyles();
  const { tournamentId } = useParams();

  const teamNameHandleChange = (event, value, index) => {
    let newData = bracketSeeds;
    newData[index] = value.props.value;

    formik.setFieldValue('bracketSeeds', newData);
  }

  const matchDateHandleChange = (value, index) => {
    let newData = dateSeeds;
    newData[index] = value.toDateString();

    formik.setFieldValue('dateSeeds', newData);
  }

  const handleOnSubmit = async (values) => {
    let bodyFormData = new FormData();

    values.bracketSeeds.map(bracketSeed => {
      const rest = teamsData.filter(team => team.name === bracketSeed);
      const id = rest.length !== 0 ? rest[0]._id : null;
      bodyFormData.append("bracketSeeds", id);
    })

    values.dateSeeds.map(dateSeed => {
      bodyFormData.append("dateSeeds", dateSeed);
    })

    if(values.winnerName){
      const winner = teamsData.filter(team => team.name === values.winnerName);

      bodyFormData.set("winner", winner[0]._id);
    }else{
      bodyFormData.set("winner", null);
    }
    
    const { status: updateTournamentBracket } = await updateTournament(bodyFormData, tournamentId);

    if (updateTournamentBracket === 200) {
      handleClose();
    }
  }

  const formik = useFormik({
    initialValues: {
      bracketSeeds,
      dateSeeds,
      winnerName
    },
    onSubmit: values => {
      handleOnSubmit(values);
    }
  });

  const { values, handleSubmit } = formik;

  const renderTextFieldBySlotSize = () => {
    const fourSlot = (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <h2 className={classes.row}>Round 1</h2>
          <Grid container className={classes.row}>
            <Grid item xs={3}>
              <CustomTextField
                name='Match 1'
                select
                label="Match 1"
                variant="outlined"
                value={values.bracketSeeds[0]}
                onChange={(event, value) => teamNameHandleChange(event, value, 0)}
                style={{ width: 300 }}
                disabled={disabledFirstMatch}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
            <Grid item xs={3}>
              <CustomTextField
                name='Match 2'
                select
                label="Match 2"
                variant="outlined"
                value={values.bracketSeeds[2]}
                onChange={(event, value) => teamNameHandleChange(event, value, 2)}
                style={{ width: 300 }}
                disabled={disabledFirstMatch}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
          </Grid>
          <Grid container className={classes.row}>
            <Grid item xs={3}>
              <CustomTextField
                name='Match 1'
                select
                label="Match 1"
                variant="outlined"
                value={values.bracketSeeds[1]}
                onChange={(event, value) => teamNameHandleChange(event, value, 1)}
                style={{ width: 300 }}
                disabled={disabledFirstMatch}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
            <Grid item xs={3}>
              <CustomTextField
                name='Match 2'
                select
                label="Match 2"
                variant="outlined"
                value={values.bracketSeeds[3]}
                onChange={(event, value) => teamNameHandleChange(event, value, 3)}
                style={{ width: 300 }}
                disabled={disabledFirstMatch}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
          </Grid>
          <Grid container className={classes.row}>
            <Grid item xs={3}>
              <CustomDatePicker
                inputVariant="outlined"
                format="MM/dd/yyyy"
                label="Match Date"
                onChange={(e) => matchDateHandleChange(e, 0)}
                value={formik.values.dateSeeds[0]}
              />
            </Grid>
            <Grid item xs={3}>
              <CustomDatePicker
                inputVariant="outlined"
                format="MM/dd/yyyy"
                label="Match Date"
                onChange={(e) => matchDateHandleChange(e, 1)}
                value={formik.values.dateSeeds[0]}
              />
            </Grid>
          </Grid>
          <h2 className={classes.row}>Round 2</h2>
          <Grid container className={classes.row}>
            <Grid item xs={3}>
              <CustomTextField
                name='Match 3'
                select
                label="Match 3"
                variant="outlined"
                value={values.bracketSeeds[4]}
                onChange={(event, value) => teamNameHandleChange(event, value, 4)}
                style={{ width: 300 }}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
          </Grid>
          <Grid container className={classes.row}>
            <Grid xs={3}>
              <CustomTextField
                name='Match 3'
                select
                label="Match 3"
                variant="outlined"
                value={values.bracketSeeds[5]}
                onChange={(event, value) => teamNameHandleChange(event, value, 5)}
                style={{ width: 300 }}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
          </Grid>
          <Grid container className={classes.row}>
            <CustomDatePicker
              inputVariant="outlined"
              format="MM/dd/yyyy"
              label="Match Date"
              onChange={(e) => matchDateHandleChange(e, 2)}
              value={formik.values.dateSeeds[0]}
            />
          </Grid>
        </MuiPickersUtilsProvider>)

    const eightSlot = (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <h2 className={classes.row}>Round 1</h2>
          <Grid container className={classes.row}>
            <Grid item xs={3}>
              <CustomTextField
                name='Match 1'
                select
                label="Match 1"
                variant="outlined"
                value={values.bracketSeeds[0]}
                onChange={(event, value) => teamNameHandleChange(event, value, 0)}
                style={{ width: 300 }}
                disabled={disabledFirstMatch}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
            <Grid item xs={3}>
              <CustomTextField
                name='Match 2'
                select
                label="Match 2"
                variant="outlined"
                value={values.bracketSeeds[2]}
                onChange={(event, value) => teamNameHandleChange(event, value, 2)}
                style={{ width: 300 }}
                disabled={disabledFirstMatch}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
            <Grid item xs={3}>
              <CustomTextField
                name='Match 3'
                select
                label="Match 3"
                variant="outlined"
                value={values.bracketSeeds[4]}
                onChange={(event, value) => teamNameHandleChange(event, value, 4)}
                style={{ width: 300 }}
                disabled={disabledFirstMatch}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
            <Grid item xs={3}>
              <CustomTextField
                name='Match 4'
                select
                label="Match 4"
                variant="outlined"
                value={values.bracketSeeds[6]}
                onChange={(event, value) => teamNameHandleChange(event, value, 6)}
                style={{ width: 300 }}
                disabled={disabledFirstMatch}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
          </Grid>
          <Grid container className={classes.row}>
            <Grid item xs={3}>
              <CustomTextField
                name='Match 1'
                select
                label="Match 1"
                variant="outlined"
                value={values.bracketSeeds[1]}
                onChange={(event, value) => teamNameHandleChange(event, value, 1)}
                style={{ width: 300 }}
                disabled={disabledFirstMatch}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
            <Grid item xs={3}>
              <CustomTextField
                name='Match 2'
                select
                label="Match 2"
                variant="outlined"
                value={values.bracketSeeds[3]}
                onChange={(event, value) => teamNameHandleChange(event, value, 3)}
                style={{ width: 300 }}
                disabled={disabledFirstMatch}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
            <Grid item xs={3}>
              <CustomTextField
                name='Match 3'
                select
                label="Match 3"
                variant="outlined"
                value={values.bracketSeeds[5]}
                onChange={(event, value) => teamNameHandleChange(event, value, 5)}
                style={{ width: 300 }}
                disabled={disabledFirstMatch}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
            <Grid item xs={3}>
              <CustomTextField
                name='Match 4'
                select
                label="Match 4"
                variant="outlined"
                value={values.bracketSeeds[7]}
                onChange={(event, value) => teamNameHandleChange(event, value, 7)}
                style={{ width: 300 }}
                disabled={disabledFirstMatch}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
          </Grid>
          <Grid container className={classes.row}>
            <Grid item xs={3}>
              <CustomDatePicker
                inputVariant="outlined"
                format="MM/dd/yyyy"
                label="Match Date"
                onChange={(e) => matchDateHandleChange(e, 0)}
                value={formik.values.dateSeeds[0]}
              />
            </Grid>
            <Grid item xs={3}>
              <CustomDatePicker
                inputVariant="outlined"
                format="MM/dd/yyyy"
                label="Match Date"
                onChange={(e) => matchDateHandleChange(e, 1)}
                value={formik.values.dateSeeds[1]}
              />
            </Grid>
            <Grid item xs={3}>
              <CustomDatePicker
                inputVariant="outlined"
                format="MM/dd/yyyy"
                label="Match Date"
                onChange={(e) => matchDateHandleChange(e, 2)}
                value={formik.values.dateSeeds[2]}
              />
            </Grid>
            <Grid item xs={3}>
              <CustomDatePicker
                inputVariant="outlined"
                format="MM/dd/yyyy"
                label="Match Date"
                onChange={(e) => matchDateHandleChange(e, 3)}
                value={formik.values.dateSeeds[3]}
              />
            </Grid>
          </Grid>
          <h2 className={classes.row}>Round 2</h2>
          <Grid container className={classes.row}>
            <Grid item xs={3}>
              <CustomTextField
                name='Match 5'
                select
                label="Match 5"
                variant="outlined"
                value={values.bracketSeeds[8]}
                onChange={(event, value) => teamNameHandleChange(event, value, 8)}
                style={{ width: 300 }}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
            <Grid item xs={3}>
              <CustomTextField
                name='Match 6'
                select
                label="Match 6"
                variant="outlined"
                value={values.bracketSeeds[10]}
                onChange={(event, value) => teamNameHandleChange(event, value, 10)}
                style={{ width: 300 }}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
          </Grid>
          <Grid container className={classes.row}>
            <Grid item xs={3}>
              <CustomTextField
                name='Match 5'
                select
                label="Match 5"
                variant="outlined"
                value={values.bracketSeeds[9]}
                onChange={(event, value) => teamNameHandleChange(event, value, 9)}
                style={{ width: 300 }}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
            <Grid item xs={3}>
              <CustomTextField
                name='Match 6'
                select
                label="Match 6"
                variant="outlined"
                value={values.bracketSeeds[11]}
                onChange={(event, value) => teamNameHandleChange(event, value, 11)}
                style={{ width: 300 }}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
          </Grid>
          <Grid container className={classes.row}>
            <Grid item xs={3}>
              <CustomDatePicker
                inputVariant="outlined"
                format="MM/dd/yyyy"
                label="Match Date"
                onChange={(e) => matchDateHandleChange(e, 4)}
                value={formik.values.dateSeeds[4]}
              />
            </Grid>
            <Grid item xs={3}>
              <CustomDatePicker
                inputVariant="outlined"
                format="MM/dd/yyyy"
                label="Match Date"
                onChange={(e) => matchDateHandleChange(e, 5)}
                value={formik.values.dateSeeds[5]}
              />
            </Grid>
          </Grid>
          <h2 className={classes.row}>Round 3</h2>
          <Grid container className={classes.row}>
            <Grid item xs={3}>
              <CustomTextField
                name='Match 7'
                select
                label="Match 7"
                variant="outlined"
                value={values.bracketSeeds[12]}
                onChange={(event, value) => teamNameHandleChange(event, value, 12)}
                style={{ width: 300 }}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
          </Grid>
          <Grid container className={classes.row}>
            <Grid xs={3}>
              <CustomTextField
                name='Match 7'
                select
                label="Match 7"
                variant="outlined"
                value={values.bracketSeeds[13]}
                onChange={(event, value) => teamNameHandleChange(event, value, 13)}
                style={{ width: 300 }}
              >
                {teamsAutocomplete.map((option) => (
                  <MenuItem key={option.name} value={option.data}>
                    {option.name}
                  </MenuItem>
                ))}
              </CustomTextField>
            </Grid>
          </Grid>
          <Grid container className={classes.row}>
            <CustomDatePicker
              inputVariant="outlined"
              format="MM/dd/yyyy"
              label="Match Date"
              onChange={(e) => matchDateHandleChange(e, 6)}
              value={formik.values.dateSeeds[6]}
            />
          </Grid>
        </MuiPickersUtilsProvider>)

    return slotSize === 4 ? fourSlot : eightSlot;
  }

  return (
    <Modal
      className={classes.modal}
      open={open}
      onClose={handleClose}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      <div className={classes.paper}>
        <h2 id="simple-modal-title" className="EditTournamentBracketModal__heading_1">Bracket Display</h2>
        <Bracket
          rounds={getBracketSeed(bracketSeeds, dateSeeds, slotSize)}
        />
        <h2 id="simple-modal-title" className="EditTournamentBracketModal__heading_2">Edit Bracket</h2>
        <form className={classes.form}>
          {renderTextFieldBySlotSize()}
          <h2 className={classes.row}>Winner of Tournament</h2>
          <div className={classes.row}>
            <CustomTextField
              name='winnerName'
              select
              label="Winner"
              variant="outlined"
              value={values.winnerName}
              onChange={formik.handleChange}
              style={{ width: 300 }}
            >
              {teamsAutocomplete.map((option) => (
                <MenuItem key={option.name} value={option.data}>
                  {option.name}
                </MenuItem>
              ))}
            </CustomTextField>
          </div>
        </form>
        <div className='selectionButton'>
          <Button onClick={handleClose} variant="contained" color="primary">
            Cancel
            </Button>
          <Button onClick={handleSubmit} variant="contained" color="secondary">
            Edit
            </Button>
        </div>
      </div>
    </Modal>
  )
}

export default EditTournamentBracketModal
