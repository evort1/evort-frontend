import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  row: {
    margin: theme.spacing(2),
  },
  form: {
    '& .MuiInputBase-root': {
      color: '#90E0EF',
      width: '100%'
    }
  },
  paper: {
    backgroundColor: "#0A1128",
    padding: theme.spacing(4, 4, 3)
  },
  modal: {
    overflowY: 'auto'
  },
}));