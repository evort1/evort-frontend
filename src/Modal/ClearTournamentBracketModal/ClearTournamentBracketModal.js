import Modal from "@material-ui/core/Modal";
import React from "react";
import {useStyles} from "./ClearTournamentBracketModalStyles";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";

const ClearTournamentBracketModal = ({
  handleClickClear, openClearTournamentBracketModal,
  handleCloseClearTournamentBracketModal} ) => {
  const classes = useStyles();

  const renderClearTournamentBracketConfirmation = () => {
    return (
      <div className={classes.paper}>
        <h2>Are you sure you want to clear the bracket ? </h2>
        <FormControl component="fieldset">
          <FormLabel className={classes.clearTournamentBracket}>This change can't be undo</FormLabel>
        </FormControl>
        <div className='selectionButton'>
          <Button
            variant="contained"
            color="secondary"
            className='cancelButton'
            onClick={handleCloseClearTournamentBracketModal}
          >
            Cancel
          </Button>
          <Button variant="contained" color="primary" onClick={handleClickClear}>
            Yes
          </Button>
        </div>
      </div>
    )
  };

  return (
    <Modal
      className={classes.modal}
      open={openClearTournamentBracketModal}
      onClose={handleCloseClearTournamentBracketModal}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      {renderClearTournamentBracketConfirmation()}
    </Modal>
  )
}
export default ClearTournamentBracketModal;