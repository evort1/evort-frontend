# evort-frontend

Prerequisites:
1. node
2. node js (?)
3. react (?)

How to run :
1. run npm start in your terminal
2. open localhost:3000 in your favorite browser.

CSS name convention: BEM
Ref: https://css-tricks.com/bem-101/#:~:text=The%20Block%2C%20Element%2C%20Modifier%20methodology,CSS%20in%20a%20given%20project.
